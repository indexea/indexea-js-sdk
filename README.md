# @indexea/sdk

indexea typescript sdk

### Install

```bash
$ npm install '@indexea/sdk'
or
$ yarn add '@indexea/sdk'
```

### Usage

```javascript
import { WidgetsApi, SearchApi } from '@indexea/sdk'


// initialize indexea openapi client
const config = {
  basePath: '/v1',
  headers: { accept: 'application/json' },
  accessToken: () => {
    return 'user token'
  },
  credentials: 'include',
  middleware: []
}

var searchApi = new SearchApi(config);

searchApi.searchWidgetSearch({ widget, index, 'test', {}, 0, 20 }).then(res => {
    console.log(res);
});
```

### Update

```bash
$ ./openapi.sh full_path_of_openapi.yml
$ yarn build
$ git add *
$ git commit -m "update version"
$ npm version patch
$ npm publish
$ git push
```
