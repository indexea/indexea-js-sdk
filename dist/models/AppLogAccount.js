"use strict";
/* tslint:disable */
/* eslint-disable */
/**
 * Indexea OpenAPI
 * 这是 Indexea 搜索服务平台的 OpenAPI，用于描述平台的所有接口信息，你可以通过这个页面来了解和在线验证平台的所有接口信息。  ### Errors  本 API 使用标准的 HTTP 状态码来指示操作成功或者失败，如果失败将会在 body 中以 JSON 格式提供详细的错误信息，如下所示：  ``` {   \"error\": 404,   \"message\": \"page not found\" } ```
 *
 * The version of the OpenAPI document: 1.0.0
 * Contact: indexea.com@gmail.com
 *
 * NOTE: This class is auto generated by OpenAPI Generator (https://openapi-generator.tech).
 * https://openapi-generator.tech
 * Do not edit the class manually.
 */
Object.defineProperty(exports, "__esModule", { value: true });
exports.AppLogAccountToJSON = exports.AppLogAccountFromJSONTyped = exports.AppLogAccountFromJSON = exports.instanceOfAppLogAccount = void 0;
var runtime_1 = require("../runtime");
/**
 * Check if a given object implements the AppLogAccount interface.
 */
function instanceOfAppLogAccount(value) {
    var isInstance = true;
    return isInstance;
}
exports.instanceOfAppLogAccount = instanceOfAppLogAccount;
function AppLogAccountFromJSON(json) {
    return AppLogAccountFromJSONTyped(json, false);
}
exports.AppLogAccountFromJSON = AppLogAccountFromJSON;
function AppLogAccountFromJSONTyped(json, ignoreDiscriminator) {
    if ((json === undefined) || (json === null)) {
        return json;
    }
    return {
        'id': !(0, runtime_1.exists)(json, 'id') ? undefined : json['id'],
        'avatar': !(0, runtime_1.exists)(json, 'avatar') ? undefined : json['avatar'],
        'name': !(0, runtime_1.exists)(json, 'name') ? undefined : json['name'],
    };
}
exports.AppLogAccountFromJSONTyped = AppLogAccountFromJSONTyped;
function AppLogAccountToJSON(value) {
    if (value === undefined) {
        return undefined;
    }
    if (value === null) {
        return null;
    }
    return {
        'id': value.id,
        'avatar': value.avatar,
        'name': value.name,
    };
}
exports.AppLogAccountToJSON = AppLogAccountToJSON;
