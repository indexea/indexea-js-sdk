/**
 * Indexea OpenAPI
 * 这是 Indexea 搜索服务平台的 OpenAPI，用于描述平台的所有接口信息，你可以通过这个页面来了解和在线验证平台的所有接口信息。  ### Errors  本 API 使用标准的 HTTP 状态码来指示操作成功或者失败，如果失败将会在 body 中以 JSON 格式提供详细的错误信息，如下所示：  ``` {   \"error\": 404,   \"message\": \"page not found\" } ```
 *
 * The version of the OpenAPI document: 1.0.0
 * Contact: indexea.com@gmail.com
 *
 * NOTE: This class is auto generated by OpenAPI Generator (https://openapi-generator.tech).
 * https://openapi-generator.tech
 * Do not edit the class manually.
 */
import * as runtime from '../runtime';
import type { ValueOfField } from '../models';
export interface IndexFieldsRequest {
    app: string;
    index: number;
}
export interface IndexUpdateFieldsRequest {
    app: string;
    index: number;
    body: object;
}
export interface IndexUpdateHtmlStripFieldsRequest {
    app: string;
    index: number;
    body: object;
}
export interface IndexValuesOfFieldRequest {
    app: string;
    index: number;
    field: string;
    size: number;
}
/**
 *
 */
export declare class FieldsApi extends runtime.BaseAPI {
    /**
     * 获取索引字段映射详情
     */
    indexFieldsRaw(requestParameters: IndexFieldsRequest, initOverrides?: RequestInit | runtime.InitOverrideFunction): Promise<runtime.ApiResponse<object>>;
    /**
     * 获取索引字段映射详情
     */
    indexFields(requestParameters: IndexFieldsRequest, initOverrides?: RequestInit | runtime.InitOverrideFunction): Promise<object>;
    /**
     * 更新索引的字段映射
     */
    indexUpdateFieldsRaw(requestParameters: IndexUpdateFieldsRequest, initOverrides?: RequestInit | runtime.InitOverrideFunction): Promise<runtime.ApiResponse<object>>;
    /**
     * 更新索引的字段映射
     */
    indexUpdateFields(requestParameters: IndexUpdateFieldsRequest, initOverrides?: RequestInit | runtime.InitOverrideFunction): Promise<object>;
    /**
     * 更新索引的HTML过滤字段列表
     */
    indexUpdateHtmlStripFieldsRaw(requestParameters: IndexUpdateHtmlStripFieldsRequest, initOverrides?: RequestInit | runtime.InitOverrideFunction): Promise<runtime.ApiResponse<object>>;
    /**
     * 更新索引的HTML过滤字段列表
     */
    indexUpdateHtmlStripFields(requestParameters: IndexUpdateHtmlStripFieldsRequest, initOverrides?: RequestInit | runtime.InitOverrideFunction): Promise<object>;
    /**
     * 获取索引字段的所有值列表
     */
    indexValuesOfFieldRaw(requestParameters: IndexValuesOfFieldRequest, initOverrides?: RequestInit | runtime.InitOverrideFunction): Promise<runtime.ApiResponse<Array<ValueOfField>>>;
    /**
     * 获取索引字段的所有值列表
     */
    indexValuesOfField(requestParameters: IndexValuesOfFieldRequest, initOverrides?: RequestInit | runtime.InitOverrideFunction): Promise<Array<ValueOfField>>;
}
