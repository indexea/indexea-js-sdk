/**
 * Indexea OpenAPI
 * 这是 Indexea 搜索服务平台的 OpenAPI，用于描述平台的所有接口信息，你可以通过这个页面来了解和在线验证平台的所有接口信息。  ### Errors  本 API 使用标准的 HTTP 状态码来指示操作成功或者失败，如果失败将会在 body 中以 JSON 格式提供详细的错误信息，如下所示：  ``` {   \"error\": 404,   \"message\": \"page not found\" } ```
 *
 * The version of the OpenAPI document: 1.0.0
 * Contact: indexea.com@gmail.com
 *
 * NOTE: This class is auto generated by OpenAPI Generator (https://openapi-generator.tech).
 * https://openapi-generator.tech
 * Do not edit the class manually.
 */
/**
 * 第三方应用详情
 * @export
 * @interface OauthAppBean
 */
export interface OauthAppBean {
    /**
     *
     * @type {number}
     * @memberof OauthAppBean
     */
    id?: number;
    /**
     *
     * @type {number}
     * @memberof OauthAppBean
     */
    account?: number;
    /**
     *
     * @type {number}
     * @memberof OauthAppBean
     */
    app?: number;
    /**
     * 三方应用标识 client id
     * @type {string}
     * @memberof OauthAppBean
     */
    ident?: string;
    /**
     *
     * @type {string}
     * @memberof OauthAppBean
     */
    name?: string;
    /**
     *
     * @type {string}
     * @memberof OauthAppBean
     */
    intro?: string;
    /**
     *
     * @type {string}
     * @memberof OauthAppBean
     */
    logo?: string;
    /**
     * 应用地址
     * @type {string}
     * @memberof OauthAppBean
     */
    url?: string;
    /**
     * 第三方应用密钥, 该值仅在创建和重置密钥时返回
     * @type {string}
     * @memberof OauthAppBean
     */
    secret?: string;
    /**
     * 回调地址
     * @type {string}
     * @memberof OauthAppBean
     */
    callback?: string;
    /**
     * 授权范围
     * @type {string}
     * @memberof OauthAppBean
     */
    scopes?: string;
    /**
     *
     * @type {Date}
     * @memberof OauthAppBean
     */
    createdAt?: Date;
    /**
     *
     * @type {Date}
     * @memberof OauthAppBean
     */
    updatedAt?: Date;
    /**
     *
     * @type {number}
     * @memberof OauthAppBean
     */
    status?: number;
}
/**
 * Check if a given object implements the OauthAppBean interface.
 */
export declare function instanceOfOauthAppBean(value: object): boolean;
export declare function OauthAppBeanFromJSON(json: any): OauthAppBean;
export declare function OauthAppBeanFromJSONTyped(json: any, ignoreDiscriminator: boolean): OauthAppBean;
export declare function OauthAppBeanToJSON(value?: OauthAppBean | null): any;
