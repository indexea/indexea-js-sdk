"use strict";
/* tslint:disable */
/* eslint-disable */
/**
 * Indexea OpenAPI
 * 这是 Indexea 搜索服务平台的 OpenAPI，用于描述平台的所有接口信息，你可以通过这个页面来了解和在线验证平台的所有接口信息。  ### Errors  本 API 使用标准的 HTTP 状态码来指示操作成功或者失败，如果失败将会在 body 中以 JSON 格式提供详细的错误信息，如下所示：  ``` {   \"error\": 404,   \"message\": \"page not found\" } ```
 *
 * The version of the OpenAPI document: 1.0.0
 * Contact: indexea.com@gmail.com
 *
 * NOTE: This class is auto generated by OpenAPI Generator (https://openapi-generator.tech).
 * https://openapi-generator.tech
 * Do not edit the class manually.
 */
Object.defineProperty(exports, "__esModule", { value: true });
exports.RecordFilterToJSON = exports.RecordFilterFromJSONTyped = exports.RecordFilterFromJSON = exports.instanceOfRecordFilter = void 0;
var runtime_1 = require("../runtime");
/**
 * Check if a given object implements the RecordFilter interface.
 */
function instanceOfRecordFilter(value) {
    var isInstance = true;
    return isInstance;
}
exports.instanceOfRecordFilter = instanceOfRecordFilter;
function RecordFilterFromJSON(json) {
    return RecordFilterFromJSONTyped(json, false);
}
exports.RecordFilterFromJSON = RecordFilterFromJSON;
function RecordFilterFromJSONTyped(json, ignoreDiscriminator) {
    if ((json === undefined) || (json === null)) {
        return json;
    }
    return {
        'filters': !(0, runtime_1.exists)(json, 'filters') ? undefined : json['filters'],
        'aggs': !(0, runtime_1.exists)(json, 'aggs') ? undefined : json['aggs'],
        'sorts': !(0, runtime_1.exists)(json, 'sorts') ? undefined : json['sorts'],
        'postFilters': !(0, runtime_1.exists)(json, 'post_filters') ? undefined : json['post_filters'],
    };
}
exports.RecordFilterFromJSONTyped = RecordFilterFromJSONTyped;
function RecordFilterToJSON(value) {
    if (value === undefined) {
        return undefined;
    }
    if (value === null) {
        return null;
    }
    return {
        'filters': value.filters,
        'aggs': value.aggs,
        'sorts': value.sorts,
        'post_filters': value.postFilters,
    };
}
exports.RecordFilterToJSON = RecordFilterToJSON;
