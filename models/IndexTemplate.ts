/* tslint:disable */
/* eslint-disable */
/**
 * Indexea OpenAPI
 * 这是 Indexea 搜索服务平台的 OpenAPI，用于描述平台的所有接口信息，你可以通过这个页面来了解和在线验证平台的所有接口信息。  ### Errors  本 API 使用标准的 HTTP 状态码来指示操作成功或者失败，如果失败将会在 body 中以 JSON 格式提供详细的错误信息，如下所示：  ``` {   \"error\": 404,   \"message\": \"page not found\" } ``` 
 *
 * The version of the OpenAPI document: 1.0.0
 * Contact: indexea.com@gmail.com
 *
 * NOTE: This class is auto generated by OpenAPI Generator (https://openapi-generator.tech).
 * https://openapi-generator.tech
 * Do not edit the class manually.
 */

import { exists, mapValues } from '../runtime';
/**
 * 索引模板
 * @export
 * @interface IndexTemplate
 */
export interface IndexTemplate {
    /**
     * 
     * @type {number}
     * @memberof IndexTemplate
     */
    id?: number;
    /**
     * 
     * @type {number}
     * @memberof IndexTemplate
     */
    account?: number;
    /**
     * 
     * @type {string}
     * @memberof IndexTemplate
     */
    name?: string;
    /**
     * 
     * @type {string}
     * @memberof IndexTemplate
     */
    intro?: string;
    /**
     * 
     * @type {object}
     * @memberof IndexTemplate
     */
    mappings?: object;
    /**
     * 
     * @type {object}
     * @memberof IndexTemplate
     */
    query?: object;
    /**
     * 
     * @type {Array<object>}
     * @memberof IndexTemplate
     */
    widgets?: Array<object>;
    /**
     * 
     * @type {Date}
     * @memberof IndexTemplate
     */
    createdAt?: Date;
    /**
     * 
     * @type {Date}
     * @memberof IndexTemplate
     */
    updatedAt?: Date;
    /**
     * 
     * @type {boolean}
     * @memberof IndexTemplate
     */
    opened?: boolean;
    /**
     * 
     * @type {number}
     * @memberof IndexTemplate
     */
    status?: number;
}

/**
 * Check if a given object implements the IndexTemplate interface.
 */
export function instanceOfIndexTemplate(value: object): boolean {
    let isInstance = true;

    return isInstance;
}

export function IndexTemplateFromJSON(json: any): IndexTemplate {
    return IndexTemplateFromJSONTyped(json, false);
}

export function IndexTemplateFromJSONTyped(json: any, ignoreDiscriminator: boolean): IndexTemplate {
    if ((json === undefined) || (json === null)) {
        return json;
    }
    return {
        
        'id': !exists(json, 'id') ? undefined : json['id'],
        'account': !exists(json, 'account') ? undefined : json['account'],
        'name': !exists(json, 'name') ? undefined : json['name'],
        'intro': !exists(json, 'intro') ? undefined : json['intro'],
        'mappings': !exists(json, 'mappings') ? undefined : json['mappings'],
        'query': !exists(json, 'query') ? undefined : json['query'],
        'widgets': !exists(json, 'widgets') ? undefined : json['widgets'],
        'createdAt': !exists(json, 'created_at') ? undefined : (new Date(json['created_at'])),
        'updatedAt': !exists(json, 'updated_at') ? undefined : (new Date(json['updated_at'])),
        'opened': !exists(json, 'opened') ? undefined : json['opened'],
        'status': !exists(json, 'status') ? undefined : json['status'],
    };
}

export function IndexTemplateToJSON(value?: IndexTemplate | null): any {
    if (value === undefined) {
        return undefined;
    }
    if (value === null) {
        return null;
    }
    return {
        
        'id': value.id,
        'account': value.account,
        'name': value.name,
        'intro': value.intro,
        'mappings': value.mappings,
        'query': value.query,
        'widgets': value.widgets,
        'created_at': value.createdAt === undefined ? undefined : (value.createdAt.toISOString()),
        'updated_at': value.updatedAt === undefined ? undefined : (value.updatedAt.toISOString()),
        'opened': value.opened,
        'status': value.status,
    };
}

