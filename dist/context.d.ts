/**
 * Search Context
 */
import type { WidgetBean } from "./models/WidgetBean";
export declare class SearchContext {
    widget: WidgetBean;
    q: string;
    page: number;
    filters: Array<SearchFilter>;
    query: number;
    sort: string;
    itemsPerPage: number;
    constructor(widget: WidgetBean, q: string, page: number, filters: Array<SearchFilter>, query: number, sort: string);
    toUrl(): string;
    static fromUrl(widget: WidgetBean, queryString: string): SearchContext;
    getFromIndex(): number;
    setq(q: string): void;
    getq(): string;
    setPage(p: number): void;
    getPage(): number;
    setQuery(q: number): void;
    getQuery(): number;
    setSort(s: string): void;
    getSort(): string;
    getFilters(): Array<SearchFilter>;
    addFilter(f: SearchFilter): void;
    deleteFilter(f: SearchFilter): void;
}
export declare class SearchFilter {
    name: string;
    value: string;
    interval: string;
    multiple: boolean;
    constructor(name: string, value: string, interval: string, multiple?: boolean);
}
