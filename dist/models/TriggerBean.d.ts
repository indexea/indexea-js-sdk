/**
 * Indexea OpenAPI
 * 这是 Indexea 搜索服务平台的 OpenAPI，用于描述平台的所有接口信息，你可以通过这个页面来了解和在线验证平台的所有接口信息。  ### Errors  本 API 使用标准的 HTTP 状态码来指示操作成功或者失败，如果失败将会在 body 中以 JSON 格式提供详细的错误信息，如下所示：  ``` {   \"error\": 404,   \"message\": \"page not found\" } ```
 *
 * The version of the OpenAPI document: 1.0.0
 * Contact: indexea.com@gmail.com
 *
 * NOTE: This class is auto generated by OpenAPI Generator (https://openapi-generator.tech).
 * https://openapi-generator.tech
 * Do not edit the class manually.
 */
/**
 * 触发器详细信息
 * @export
 * @interface TriggerBean
 */
export interface TriggerBean {
    /**
     *
     * @type {number}
     * @memberof TriggerBean
     */
    app?: number;
    /**
     *
     * @type {string}
     * @memberof TriggerBean
     */
    url?: string;
    /**
     *
     * @type {string}
     * @memberof TriggerBean
     */
    triggers?: string;
    /**
     *
     * @type {string}
     * @memberof TriggerBean
     */
    password?: string;
    /**
     *
     * @type {Array<string>}
     * @memberof TriggerBean
     */
    allTriggers?: Array<string>;
    /**
     *
     * @type {boolean}
     * @memberof TriggerBean
     */
    enabled?: boolean;
    /**
     *
     * @type {Date}
     * @memberof TriggerBean
     */
    createdAt?: Date;
}
/**
 * Check if a given object implements the TriggerBean interface.
 */
export declare function instanceOfTriggerBean(value: object): boolean;
export declare function TriggerBeanFromJSON(json: any): TriggerBean;
export declare function TriggerBeanFromJSONTyped(json: any, ignoreDiscriminator: boolean): TriggerBean;
export declare function TriggerBeanToJSON(value?: TriggerBean | null): any;
