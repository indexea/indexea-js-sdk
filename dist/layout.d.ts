/**
 * widget layout manager
 * example：
    <layout>
        <row>
            <col width='6'>
                <control type='input'/>
            </col>
            <col width='6'>
                <control type='hotwords'/>
            </col>
        </row>
        <row>
            <col width='9'>
                <control type='results'/>
            </col>
            <col width='3'>
                <control type='aggregation'/>
                <control type='aggregation'/>
            </col>
        </row>
        <row>
            <col width='9'>
                <control type='pagination'/>
            </col>
        </row>
    </layout>
 */
export declare class WidgetLayout {
    private xml;
    private constructor();
    /**
     * parse layout xml string
     * @param xmlstring
     * @returns
     */
    static parse(xmlstring: string): WidgetLayout;
    /**
     * turn to layout xml string
     * @returns
     */
    toString(): string;
    /**
     * check if exists a control named `ctrl_name`
     * @param ctrl_name
     * @returns
     */
    control(ctrl_name: string): any;
    /**
     * get all rows in layout
     * @returns
     */
    rows(): any;
    /**
     * append node to position
     * @param node
     * @param row
     * @param col
     * @returns
     */
    append(node: any, row?: number, col?: number): WidgetLayout;
    /**
     * remove node from position
     * @param row
     * @param col
     * @param index
     * @returns
     */
    remove(row: number, col: number, index: number): WidgetLayout;
    /**
     * get control attribute with name `attr_name`
     * @param ctrl_name
     * @param attr_name
     * @returns
     */
    getAttribute(ctrl_name: string, attr_name: string): any;
    /**
     * get attributes of control
     * @param ctrl_name
     */
    getAttributes(ctrl_name?: string): any;
    /**
     * set control attributes
     * @param {row}} ctrl_name
     * @param {attrs} attrs
     */
    setAttributes(ctrl_name: string, attrs: any): void;
    /**
     * set control attributes
     * @param {row}} row
     * @param {col} col
     * @param {index} index
     * @param {attrs} attrs
     */
    setAttributesByPos(row: number, col: number, index: number, attrs: any): void;
    private appendToNewRow;
    private appendToNewCol;
}
