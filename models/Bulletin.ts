/* tslint:disable */
/* eslint-disable */
/**
 * Indexea OpenAPI
 * 这是 Indexea 搜索服务平台的 OpenAPI，用于描述平台的所有接口信息，你可以通过这个页面来了解和在线验证平台的所有接口信息。  ### Errors  本 API 使用标准的 HTTP 状态码来指示操作成功或者失败，如果失败将会在 body 中以 JSON 格式提供详细的错误信息，如下所示：  ``` {   \"error\": 404,   \"message\": \"page not found\" } ``` 
 *
 * The version of the OpenAPI document: 1.0.0
 * Contact: indexea.com@gmail.com
 *
 * NOTE: This class is auto generated by OpenAPI Generator (https://openapi-generator.tech).
 * https://openapi-generator.tech
 * Do not edit the class manually.
 */

import { exists, mapValues } from '../runtime';
/**
 * 
 * @export
 * @interface Bulletin
 */
export interface Bulletin {
    /**
     * 
     * @type {number}
     * @memberof Bulletin
     */
    id?: number;
    /**
     * 
     * @type {string}
     * @memberof Bulletin
     */
    level?: BulletinLevelEnum;
    /**
     * 
     * @type {number}
     * @memberof Bulletin
     */
    scope?: number;
    /**
     * 
     * @type {string}
     * @memberof Bulletin
     */
    title?: string;
    /**
     * 
     * @type {string}
     * @memberof Bulletin
     */
    content?: string;
    /**
     * 
     * @type {Date}
     * @memberof Bulletin
     */
    createdAt?: Date;
    /**
     * 
     * @type {Date}
     * @memberof Bulletin
     */
    startAt?: Date;
    /**
     * 
     * @type {Date}
     * @memberof Bulletin
     */
    expiredAt?: Date;
    /**
     * 
     * @type {boolean}
     * @memberof Bulletin
     */
    closable?: boolean;
}


/**
 * @export
 */
export const BulletinLevelEnum = {
    Success: 'success',
    Warning: 'warning',
    Danger: 'danger'
} as const;
export type BulletinLevelEnum = typeof BulletinLevelEnum[keyof typeof BulletinLevelEnum];


/**
 * Check if a given object implements the Bulletin interface.
 */
export function instanceOfBulletin(value: object): boolean {
    let isInstance = true;

    return isInstance;
}

export function BulletinFromJSON(json: any): Bulletin {
    return BulletinFromJSONTyped(json, false);
}

export function BulletinFromJSONTyped(json: any, ignoreDiscriminator: boolean): Bulletin {
    if ((json === undefined) || (json === null)) {
        return json;
    }
    return {
        
        'id': !exists(json, 'id') ? undefined : json['id'],
        'level': !exists(json, 'level') ? undefined : json['level'],
        'scope': !exists(json, 'scope') ? undefined : json['scope'],
        'title': !exists(json, 'title') ? undefined : json['title'],
        'content': !exists(json, 'content') ? undefined : json['content'],
        'createdAt': !exists(json, 'created_at') ? undefined : (new Date(json['created_at'])),
        'startAt': !exists(json, 'start_at') ? undefined : (new Date(json['start_at'])),
        'expiredAt': !exists(json, 'expired_at') ? undefined : (new Date(json['expired_at'])),
        'closable': !exists(json, 'closable') ? undefined : json['closable'],
    };
}

export function BulletinToJSON(value?: Bulletin | null): any {
    if (value === undefined) {
        return undefined;
    }
    if (value === null) {
        return null;
    }
    return {
        
        'id': value.id,
        'level': value.level,
        'scope': value.scope,
        'title': value.title,
        'content': value.content,
        'created_at': value.createdAt === undefined ? undefined : (value.createdAt.toISOString()),
        'start_at': value.startAt === undefined ? undefined : (value.startAt.toISOString()),
        'expired_at': value.expiredAt === undefined ? undefined : (value.expiredAt.toISOString()),
        'closable': value.closable,
    };
}

