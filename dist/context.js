"use strict";
/**
 * Search Context
 */
Object.defineProperty(exports, "__esModule", { value: true });
exports.SearchFilter = exports.SearchContext = void 0;
var layout_1 = require("./layout");
var SearchContext = /** @class */ (function () {
    function SearchContext(widget, q, page, filters, query, sort) {
        this.q = "";
        this.page = 1;
        this.filters = [];
        this.query = 0;
        this.sort = "";
        this.itemsPerPage = 20;
        this.widget = widget;
        this.q = q;
        this.page = page;
        this.filters = filters;
        this.query = query;
        this.sort = sort;
        //@ts-ignore
        var ps = layout_1.WidgetLayout.parse(widget.layout).getAttribute('results', 'pagesize-' + this.query);
        this.itemsPerPage = +ps || 20;
    }
    SearchContext.prototype.toUrl = function () {
        var url = "";
        if (this.q)
            url += "&q=" + encodeURIComponent(this.q);
        //@ts-ignore
        if (this.query > 0 && this.query != this.widget.queries[0].id)
            url += "&i=" + this.query;
        if (this.filters.length > 0)
            url += "&" + this.filters.map(function (f) { return encodeURIComponent(f.name) + "=" + encodeURIComponent(f.value); }).join("&");
        if (this.page > 1)
            url += "&p=" + this.page;
        if (this.sort)
            url += "&sort_by_f=" + encodeURIComponent(this.sort);
        return url ? url.substring(1) : url;
    };
    SearchContext.fromUrl = function (widget, queryString) {
        var PNAMES = ["q", "p", "i", "sort_by_f"];
        var params = new URLSearchParams(queryString);
        var q = params.get("q") || "";
        //@ts-ignore
        var page = +params.get("p") || 1;
        //@ts-ignore
        var query = +params.get("i") || widget.queries[0].id;
        var sort = params.get("sort_by_f") || "";
        var filters = [];
        params.forEach(function (value, key) {
            if (PNAMES.indexOf(key) < 0) {
                filters.push(new SearchFilter(key, value, params.get(key + ".interval") || ""));
            }
        });
        //@ts-ignore
        var context = new SearchContext(widget, q, page, filters, query, sort);
        return context;
    };
    SearchContext.prototype.getFromIndex = function () {
        return (this.page - 1) * this.itemsPerPage;
    };
    SearchContext.prototype.setq = function (q) {
        this.q = q;
    };
    SearchContext.prototype.getq = function () {
        return this.q;
    };
    SearchContext.prototype.setPage = function (p) {
        this.page = p;
    };
    SearchContext.prototype.getPage = function () {
        return this.page;
    };
    SearchContext.prototype.setQuery = function (q) {
        this.query = q;
    };
    SearchContext.prototype.getQuery = function () {
        return this.query;
    };
    SearchContext.prototype.setSort = function (s) {
        this.sort = s;
    };
    SearchContext.prototype.getSort = function () {
        return this.sort;
    };
    SearchContext.prototype.getFilters = function () {
        return this.filters;
    };
    SearchContext.prototype.addFilter = function (f) {
        if (f.multiple) {
            this.filters.push(f);
        }
    };
    SearchContext.prototype.deleteFilter = function (f) {
        this.filters = this.filters.filter(function (tf) { return tf.name != f.name || tf.value != f.value; });
    };
    return SearchContext;
}());
exports.SearchContext = SearchContext;
var SearchFilter = /** @class */ (function () {
    function SearchFilter(name, value, interval, multiple) {
        if (multiple === void 0) { multiple = false; }
        this.name = "";
        this.value = "";
        this.interval = "";
        this.multiple = false;
        this.name = name;
        this.value = value;
        this.interval = interval;
        this.multiple = multiple;
    }
    return SearchFilter;
}());
exports.SearchFilter = SearchFilter;
