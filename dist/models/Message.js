"use strict";
/* tslint:disable */
/* eslint-disable */
/**
 * Indexea OpenAPI
 * 这是 Indexea 搜索服务平台的 OpenAPI，用于描述平台的所有接口信息，你可以通过这个页面来了解和在线验证平台的所有接口信息。  ### Errors  本 API 使用标准的 HTTP 状态码来指示操作成功或者失败，如果失败将会在 body 中以 JSON 格式提供详细的错误信息，如下所示：  ``` {   \"error\": 404,   \"message\": \"page not found\" } ```
 *
 * The version of the OpenAPI document: 1.0.0
 * Contact: indexea.com@gmail.com
 *
 * NOTE: This class is auto generated by OpenAPI Generator (https://openapi-generator.tech).
 * https://openapi-generator.tech
 * Do not edit the class manually.
 */
Object.defineProperty(exports, "__esModule", { value: true });
exports.MessageToJSON = exports.MessageFromJSONTyped = exports.MessageFromJSON = exports.instanceOfMessage = void 0;
var runtime_1 = require("../runtime");
/**
 * Check if a given object implements the Message interface.
 */
function instanceOfMessage(value) {
    var isInstance = true;
    return isInstance;
}
exports.instanceOfMessage = instanceOfMessage;
function MessageFromJSON(json) {
    return MessageFromJSONTyped(json, false);
}
exports.MessageFromJSON = MessageFromJSON;
function MessageFromJSONTyped(json, ignoreDiscriminator) {
    if ((json === undefined) || (json === null)) {
        return json;
    }
    return {
        'id': !(0, runtime_1.exists)(json, 'id') ? undefined : json['id'],
        'account': !(0, runtime_1.exists)(json, 'account') ? undefined : json['account'],
        'sender': !(0, runtime_1.exists)(json, 'sender') ? undefined : json['sender'],
        'receiver': !(0, runtime_1.exists)(json, 'receiver') ? undefined : json['receiver'],
        'name': !(0, runtime_1.exists)(json, 'name') ? undefined : json['name'],
        'avater': !(0, runtime_1.exists)(json, 'avater') ? undefined : json['avater'],
        'msg': !(0, runtime_1.exists)(json, 'msg') ? undefined : json['msg'],
        'type': !(0, runtime_1.exists)(json, 'type') ? undefined : json['type'],
        'msgid': !(0, runtime_1.exists)(json, 'msgid') ? undefined : json['msgid'],
        'createdAt': !(0, runtime_1.exists)(json, 'created_at') ? undefined : (new Date(json['created_at'])),
        'readAt': !(0, runtime_1.exists)(json, 'read_at') ? undefined : (new Date(json['read_at'])),
        'status': !(0, runtime_1.exists)(json, 'status') ? undefined : json['status'],
    };
}
exports.MessageFromJSONTyped = MessageFromJSONTyped;
function MessageToJSON(value) {
    if (value === undefined) {
        return undefined;
    }
    if (value === null) {
        return null;
    }
    return {
        'id': value.id,
        'account': value.account,
        'sender': value.sender,
        'receiver': value.receiver,
        'name': value.name,
        'avater': value.avater,
        'msg': value.msg,
        'type': value.type,
        'msgid': value.msgid,
        'created_at': value.createdAt === undefined ? undefined : (value.createdAt.toISOString()),
        'read_at': value.readAt === undefined ? undefined : (value.readAt.toISOString()),
        'status': value.status,
    };
}
exports.MessageToJSON = MessageToJSON;
