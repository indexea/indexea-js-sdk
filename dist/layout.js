"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
exports.WidgetLayout = void 0;
/**
 * widget layout manager
 * example：
    <layout>
        <row>
            <col width='6'>
                <control type='input'/>
            </col>
            <col width='6'>
                <control type='hotwords'/>
            </col>
        </row>
        <row>
            <col width='9'>
                <control type='results'/>
            </col>
            <col width='3'>
                <control type='aggregation'/>
                <control type='aggregation'/>
            </col>
        </row>
        <row>
            <col width='9'>
                <control type='pagination'/>
            </col>
        </row>
    </layout>
 */
var WidgetLayout = /** @class */ (function () {
    function WidgetLayout(xml) {
        this.xml = xml;
    }
    /**
     * parse layout xml string
     * @param xmlstring
     * @returns
     */
    WidgetLayout.parse = function (xmlstring) {
        var parser = new DOMParser();
        var xml = parser.parseFromString(xmlstring, 'text/xml');
        return new WidgetLayout(xml);
    };
    /**
     * turn to layout xml string
     * @returns
     */
    WidgetLayout.prototype.toString = function () {
        var oSerializer = new XMLSerializer();
        var sXML = oSerializer.serializeToString(this.xml);
        return sXML;
    };
    /**
     * check if exists a control named `ctrl_name`
     * @param ctrl_name
     * @returns
     */
    WidgetLayout.prototype.control = function (ctrl_name) {
        return this.xml.querySelector("control[type='".concat(ctrl_name, "']"));
    };
    /**
     * get all rows in layout
     * @returns
     */
    WidgetLayout.prototype.rows = function () {
        return this.xml.getElementsByTagName("row");
    };
    /**
     * append node to position
     * @param node
     * @param row
     * @param col
     * @returns
     */
    WidgetLayout.prototype.append = function (node, row, col) {
        var _a;
        if (row === void 0) { row = 0; }
        if (col === void 0) { col = 0; }
        if (this.control(node.type) != null)
            return this;
        //get position of row and col
        var theNode;
        var rowNodes = this.xml.getElementsByTagName('row');
        if (row >= 0 && row < rowNodes.length) {
            var rowNode = rowNodes[row];
            var colNodes = rowNode.getElementsByTagName('col');
            if (col >= 0 && col < colNodes.length) {
                theNode = colNodes[col];
            }
        }
        var elem = this.xml.createElement('control');
        elem.setAttribute('type', node.type);
        if (theNode) {
            //拖入设计器的某个元素之上
            var cols = (_a = theNode.parentNode) === null || _a === void 0 ? void 0 : _a.getElementsByTagName('col');
            var lastWidth = parseInt(cols[cols.length - 1].getAttribute('width'));
            var totalWidth = 0;
            for (var i = 0; i < cols.length; i++) {
                totalWidth += parseInt(cols[i].getAttribute('width'));
            }
            if (totalWidth + node.width > 12) {
                if (node.width == lastWidth)
                    cols[cols.length - 1].appendChild(elem);
                else
                    this.appendToNewRow(theNode, elem, node.width);
            }
            else {
                this.appendToNewCol(theNode, elem, node.width);
            }
        }
        else {
            //拖入设计器的空白处
            this.appendToNewRow(theNode, elem, node.width);
        }
        return this;
    };
    /**
     * remove node from position
     * @param row
     * @param col
     * @param index
     * @returns
     */
    WidgetLayout.prototype.remove = function (row, col, index) {
        var _a;
        var rowNodes = this.xml.getElementsByTagName('row');
        if (row < rowNodes.length) {
            var rowNode = rowNodes[row];
            var colNodes = rowNode.getElementsByTagName('col');
            if (col < colNodes.length) {
                colNodes[col].removeChild(colNodes[col].childNodes[index]);
                //如果该列已经没有子节点了，则删除该列
                if (!colNodes[col].hasChildNodes()) {
                    rowNode.removeChild(colNodes[col]);
                }
                //如果该行已经没有子节点了，则删除该行
                if (!rowNode.hasChildNodes()) {
                    (_a = rowNode.parentNode) === null || _a === void 0 ? void 0 : _a.removeChild(rowNode);
                }
            }
        }
        return this;
    };
    /**
     * get control attribute with name `attr_name`
     * @param ctrl_name
     * @param attr_name
     * @returns
     */
    WidgetLayout.prototype.getAttribute = function (ctrl_name, attr_name) {
        var ctl = this.xml.querySelector("control[type='".concat(ctrl_name, "']"));
        return ctl ? ctl.getAttribute(attr_name) : null;
    };
    /**
     * get attributes of control
     * @param ctrl_name
     */
    WidgetLayout.prototype.getAttributes = function (ctrl_name) {
        var _a, _b;
        if (ctrl_name === void 0) { ctrl_name = ""; }
        var attrs = ctrl_name
            ? (_a = this.xml.querySelector("control[type='".concat(ctrl_name, "']"))) === null || _a === void 0 ? void 0 : _a.attributes
            : (_b = this.xml.querySelector('layout')) === null || _b === void 0 ? void 0 : _b.attributes;
        var props = {};
        if (attrs)
            for (var i = 0; i < attrs.length; i++) {
                var att = attrs.item(i);
                if (att) {
                    if (att.value === 'true')
                        props[att.name] = true;
                    else if (att.value === 'false')
                        props[att.name] = false;
                    else if (att.value)
                        props[att.name] = att.value;
                }
            }
        return props;
    };
    /**
     * set control attributes
     * @param {row}} ctrl_name
     * @param {attrs} attrs
     */
    WidgetLayout.prototype.setAttributes = function (ctrl_name, attrs) {
        var ctrl = this.xml.querySelector("control[type='".concat(ctrl_name, "']"));
        if (ctrl) {
            for (var key in attrs) {
                ctrl.setAttribute(key, attrs[key]);
            }
        }
    };
    /**
     * set control attributes
     * @param {row}} row
     * @param {col} col
     * @param {index} index
     * @param {attrs} attrs
     */
    WidgetLayout.prototype.setAttributesByPos = function (row, col, index, attrs) {
        var rowNodes = this.xml.getElementsByTagName('row');
        if (row < rowNodes.length) {
            var rowNode = rowNodes[row];
            var colNodes = rowNode.getElementsByTagName('col');
            if (col < colNodes.length) {
                if (index < colNodes[col].childNodes.length) {
                    var controls = colNodes[col].getElementsByTagName('control');
                    if (index < controls.length) {
                        var control = controls[index];
                        for (var key in attrs) {
                            control.setAttribute(key, attrs[key]);
                        }
                    }
                }
            }
        }
    };
    WidgetLayout.prototype.appendToNewRow = function (node, elem, width) {
        var row = this.xml.createElement('row');
        var col = this.xml.createElement('col');
        col.setAttribute('width', width + "");
        col.appendChild(elem);
        row.appendChild(col);
        this.xml.getElementsByTagName('layout')[0].appendChild(row);
    };
    WidgetLayout.prototype.appendToNewCol = function (node, elem, width) {
        var col = this.xml.createElement('col');
        col.setAttribute('width', width + "");
        col.appendChild(elem);
        node.parentNode.appendChild(col);
    };
    return WidgetLayout;
}());
exports.WidgetLayout = WidgetLayout;
