/* tslint:disable */
/* eslint-disable */
/**
 * Indexea OpenAPI
 * 这是 Indexea 搜索服务平台的 OpenAPI，用于描述平台的所有接口信息，你可以通过这个页面来了解和在线验证平台的所有接口信息。  ### Errors  本 API 使用标准的 HTTP 状态码来指示操作成功或者失败，如果失败将会在 body 中以 JSON 格式提供详细的错误信息，如下所示：  ``` {   \"error\": 404,   \"message\": \"page not found\" } ``` 
 *
 * The version of the OpenAPI document: 1.0.0
 * Contact: indexea.com@gmail.com
 *
 * NOTE: This class is auto generated by OpenAPI Generator (https://openapi-generator.tech).
 * https://openapi-generator.tech
 * Do not edit the class manually.
 */

import { exists, mapValues } from '../runtime';
/**
 * 分词的条目
 * @export
 * @interface AnalyzeToken
 */
export interface AnalyzeToken {
    /**
     * 
     * @type {string}
     * @memberof AnalyzeToken
     */
    token?: string;
    /**
     * 
     * @type {number}
     * @memberof AnalyzeToken
     */
    startOffset?: number;
    /**
     * 
     * @type {number}
     * @memberof AnalyzeToken
     */
    endOffset?: number;
    /**
     * 
     * @type {string}
     * @memberof AnalyzeToken
     */
    type?: string;
    /**
     * 
     * @type {number}
     * @memberof AnalyzeToken
     */
    position?: number;
}

/**
 * Check if a given object implements the AnalyzeToken interface.
 */
export function instanceOfAnalyzeToken(value: object): boolean {
    let isInstance = true;

    return isInstance;
}

export function AnalyzeTokenFromJSON(json: any): AnalyzeToken {
    return AnalyzeTokenFromJSONTyped(json, false);
}

export function AnalyzeTokenFromJSONTyped(json: any, ignoreDiscriminator: boolean): AnalyzeToken {
    if ((json === undefined) || (json === null)) {
        return json;
    }
    return {
        
        'token': !exists(json, 'token') ? undefined : json['token'],
        'startOffset': !exists(json, 'start_offset') ? undefined : json['start_offset'],
        'endOffset': !exists(json, 'end_offset') ? undefined : json['end_offset'],
        'type': !exists(json, 'type') ? undefined : json['type'],
        'position': !exists(json, 'position') ? undefined : json['position'],
    };
}

export function AnalyzeTokenToJSON(value?: AnalyzeToken | null): any {
    if (value === undefined) {
        return undefined;
    }
    if (value === null) {
        return null;
    }
    return {
        
        'token': value.token,
        'start_offset': value.startOffset,
        'end_offset': value.endOffset,
        'type': value.type,
        'position': value.position,
    };
}

