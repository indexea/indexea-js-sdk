/* tslint:disable */
/* eslint-disable */
/**
 * Indexea OpenAPI
 * 这是 Indexea 搜索服务平台的 OpenAPI，用于描述平台的所有接口信息，你可以通过这个页面来了解和在线验证平台的所有接口信息。  ### Errors  本 API 使用标准的 HTTP 状态码来指示操作成功或者失败，如果失败将会在 body 中以 JSON 格式提供详细的错误信息，如下所示：  ``` {   \"error\": 404,   \"message\": \"page not found\" } ``` 
 *
 * The version of the OpenAPI document: 1.0.0
 * Contact: indexea.com@gmail.com
 *
 * NOTE: This class is auto generated by OpenAPI Generator (https://openapi-generator.tech).
 * https://openapi-generator.tech
 * Do not edit the class manually.
 */

import { exists, mapValues } from '../runtime';
/**
 * 
 * @export
 * @interface WidgetLogo
 */
export interface WidgetLogo {
    /**
     * 
     * @type {string}
     * @memberof WidgetLogo
     */
    logo?: string;
}

/**
 * Check if a given object implements the WidgetLogo interface.
 */
export function instanceOfWidgetLogo(value: object): boolean {
    let isInstance = true;

    return isInstance;
}

export function WidgetLogoFromJSON(json: any): WidgetLogo {
    return WidgetLogoFromJSONTyped(json, false);
}

export function WidgetLogoFromJSONTyped(json: any, ignoreDiscriminator: boolean): WidgetLogo {
    if ((json === undefined) || (json === null)) {
        return json;
    }
    return {
        
        'logo': !exists(json, 'logo') ? undefined : json['logo'],
    };
}

export function WidgetLogoToJSON(value?: WidgetLogo | null): any {
    if (value === undefined) {
        return undefined;
    }
    if (value === null) {
        return null;
    }
    return {
        
        'logo': value.logo,
    };
}

