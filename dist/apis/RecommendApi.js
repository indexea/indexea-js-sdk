"use strict";
/* tslint:disable */
/* eslint-disable */
/**
 * Indexea OpenAPI
 * 这是 Indexea 搜索服务平台的 OpenAPI，用于描述平台的所有接口信息，你可以通过这个页面来了解和在线验证平台的所有接口信息。  ### Errors  本 API 使用标准的 HTTP 状态码来指示操作成功或者失败，如果失败将会在 body 中以 JSON 格式提供详细的错误信息，如下所示：  ``` {   \"error\": 404,   \"message\": \"page not found\" } ```
 *
 * The version of the OpenAPI document: 1.0.0
 * Contact: indexea.com@gmail.com
 *
 * NOTE: This class is auto generated by OpenAPI Generator (https://openapi-generator.tech).
 * https://openapi-generator.tech
 * Do not edit the class manually.
 */
var __extends = (this && this.__extends) || (function () {
    var extendStatics = function (d, b) {
        extendStatics = Object.setPrototypeOf ||
            ({ __proto__: [] } instanceof Array && function (d, b) { d.__proto__ = b; }) ||
            function (d, b) { for (var p in b) if (Object.prototype.hasOwnProperty.call(b, p)) d[p] = b[p]; };
        return extendStatics(d, b);
    };
    return function (d, b) {
        if (typeof b !== "function" && b !== null)
            throw new TypeError("Class extends value " + String(b) + " is not a constructor or null");
        extendStatics(d, b);
        function __() { this.constructor = d; }
        d.prototype = b === null ? Object.create(b) : (__.prototype = b.prototype, new __());
    };
})();
var __createBinding = (this && this.__createBinding) || (Object.create ? (function(o, m, k, k2) {
    if (k2 === undefined) k2 = k;
    var desc = Object.getOwnPropertyDescriptor(m, k);
    if (!desc || ("get" in desc ? !m.__esModule : desc.writable || desc.configurable)) {
      desc = { enumerable: true, get: function() { return m[k]; } };
    }
    Object.defineProperty(o, k2, desc);
}) : (function(o, m, k, k2) {
    if (k2 === undefined) k2 = k;
    o[k2] = m[k];
}));
var __setModuleDefault = (this && this.__setModuleDefault) || (Object.create ? (function(o, v) {
    Object.defineProperty(o, "default", { enumerable: true, value: v });
}) : function(o, v) {
    o["default"] = v;
});
var __importStar = (this && this.__importStar) || function (mod) {
    if (mod && mod.__esModule) return mod;
    var result = {};
    if (mod != null) for (var k in mod) if (k !== "default" && Object.prototype.hasOwnProperty.call(mod, k)) __createBinding(result, mod, k);
    __setModuleDefault(result, mod);
    return result;
};
var __awaiter = (this && this.__awaiter) || function (thisArg, _arguments, P, generator) {
    function adopt(value) { return value instanceof P ? value : new P(function (resolve) { resolve(value); }); }
    return new (P || (P = Promise))(function (resolve, reject) {
        function fulfilled(value) { try { step(generator.next(value)); } catch (e) { reject(e); } }
        function rejected(value) { try { step(generator["throw"](value)); } catch (e) { reject(e); } }
        function step(result) { result.done ? resolve(result.value) : adopt(result.value).then(fulfilled, rejected); }
        step((generator = generator.apply(thisArg, _arguments || [])).next());
    });
};
var __generator = (this && this.__generator) || function (thisArg, body) {
    var _ = { label: 0, sent: function() { if (t[0] & 1) throw t[1]; return t[1]; }, trys: [], ops: [] }, f, y, t, g;
    return g = { next: verb(0), "throw": verb(1), "return": verb(2) }, typeof Symbol === "function" && (g[Symbol.iterator] = function() { return this; }), g;
    function verb(n) { return function (v) { return step([n, v]); }; }
    function step(op) {
        if (f) throw new TypeError("Generator is already executing.");
        while (g && (g = 0, op[0] && (_ = 0)), _) try {
            if (f = 1, y && (t = op[0] & 2 ? y["return"] : op[0] ? y["throw"] || ((t = y["return"]) && t.call(y), 0) : y.next) && !(t = t.call(y, op[1])).done) return t;
            if (y = 0, t) op = [op[0] & 2, t.value];
            switch (op[0]) {
                case 0: case 1: t = op; break;
                case 4: _.label++; return { value: op[1], done: false };
                case 5: _.label++; y = op[1]; op = [0]; continue;
                case 7: op = _.ops.pop(); _.trys.pop(); continue;
                default:
                    if (!(t = _.trys, t = t.length > 0 && t[t.length - 1]) && (op[0] === 6 || op[0] === 2)) { _ = 0; continue; }
                    if (op[0] === 3 && (!t || (op[1] > t[0] && op[1] < t[3]))) { _.label = op[1]; break; }
                    if (op[0] === 6 && _.label < t[1]) { _.label = t[1]; t = op; break; }
                    if (t && _.label < t[2]) { _.label = t[2]; _.ops.push(op); break; }
                    if (t[2]) _.ops.pop();
                    _.trys.pop(); continue;
            }
            op = body.call(thisArg, _);
        } catch (e) { op = [6, e]; y = 0; } finally { f = t = 0; }
        if (op[0] & 5) throw op[1]; return { value: op[0] ? op[1] : void 0, done: true };
    }
};
Object.defineProperty(exports, "__esModule", { value: true });
exports.RecommendApi = void 0;
var runtime = __importStar(require("../runtime"));
var models_1 = require("../models");
/**
 *
 */
var RecommendApi = /** @class */ (function (_super) {
    __extends(RecommendApi, _super);
    function RecommendApi() {
        return _super !== null && _super.apply(this, arguments) || this;
    }
    /**
     * 该接口主要用于记录用户对推荐结果的点击行为
     * 推荐结果点击行为收集
     */
    RecommendApi.prototype.recommendClickRaw = function (requestParameters, initOverrides) {
        return __awaiter(this, void 0, void 0, function () {
            var queryParameters, headerParameters, response;
            return __generator(this, function (_a) {
                switch (_a.label) {
                    case 0:
                        if (requestParameters.ident === null || requestParameters.ident === undefined) {
                            throw new runtime.RequiredError('ident', 'Required parameter requestParameters.ident was null or undefined when calling recommendClick.');
                        }
                        if (requestParameters.actionId === null || requestParameters.actionId === undefined) {
                            throw new runtime.RequiredError('actionId', 'Required parameter requestParameters.actionId was null or undefined when calling recommendClick.');
                        }
                        if (requestParameters.docId === null || requestParameters.docId === undefined) {
                            throw new runtime.RequiredError('docId', 'Required parameter requestParameters.docId was null or undefined when calling recommendClick.');
                        }
                        queryParameters = {};
                        if (requestParameters.actionId !== undefined) {
                            queryParameters['action_id'] = requestParameters.actionId;
                        }
                        if (requestParameters.docId !== undefined) {
                            queryParameters['doc_id'] = requestParameters.docId;
                        }
                        headerParameters = {};
                        if (requestParameters.xToken !== undefined && requestParameters.xToken !== null) {
                            headerParameters['x-token'] = String(requestParameters.xToken);
                        }
                        if (requestParameters.userid !== undefined && requestParameters.userid !== null) {
                            headerParameters['userid'] = String(requestParameters.userid);
                        }
                        return [4 /*yield*/, this.request({
                                path: "/recommend/{ident}/click".replace("{".concat("ident", "}"), encodeURIComponent(String(requestParameters.ident))),
                                method: 'POST',
                                headers: headerParameters,
                                query: queryParameters,
                            }, initOverrides)];
                    case 1:
                        response = _a.sent();
                        return [2 /*return*/, new runtime.TextApiResponse(response)];
                }
            });
        });
    };
    /**
     * 该接口主要用于记录用户对推荐结果的点击行为
     * 推荐结果点击行为收集
     */
    RecommendApi.prototype.recommendClick = function (requestParameters, initOverrides) {
        return __awaiter(this, void 0, void 0, function () {
            var response;
            return __generator(this, function (_a) {
                switch (_a.label) {
                    case 0: return [4 /*yield*/, this.recommendClickRaw(requestParameters, initOverrides)];
                    case 1:
                        response = _a.sent();
                        return [4 /*yield*/, response.value()];
                    case 2: return [2 /*return*/, _a.sent()];
                }
            });
        });
    };
    /**
     * 创建新的推荐
     */
    RecommendApi.prototype.recommendCreateRaw = function (requestParameters, initOverrides) {
        return __awaiter(this, void 0, void 0, function () {
            var queryParameters, headerParameters, token, tokenString, response;
            return __generator(this, function (_a) {
                switch (_a.label) {
                    case 0:
                        if (requestParameters.app === null || requestParameters.app === undefined) {
                            throw new runtime.RequiredError('app', 'Required parameter requestParameters.app was null or undefined when calling recommendCreate.');
                        }
                        if (requestParameters.recommendBean === null || requestParameters.recommendBean === undefined) {
                            throw new runtime.RequiredError('recommendBean', 'Required parameter requestParameters.recommendBean was null or undefined when calling recommendCreate.');
                        }
                        queryParameters = {};
                        headerParameters = {};
                        headerParameters['Content-Type'] = 'application/json';
                        if (!(this.configuration && this.configuration.accessToken)) return [3 /*break*/, 2];
                        token = this.configuration.accessToken;
                        return [4 /*yield*/, token("TokenAuth", [])];
                    case 1:
                        tokenString = _a.sent();
                        if (tokenString) {
                            headerParameters["Authorization"] = "Bearer ".concat(tokenString);
                        }
                        _a.label = 2;
                    case 2: return [4 /*yield*/, this.request({
                            path: "/recommends/{app}".replace("{".concat("app", "}"), encodeURIComponent(String(requestParameters.app))),
                            method: 'POST',
                            headers: headerParameters,
                            query: queryParameters,
                            body: (0, models_1.RecommendBeanToJSON)(requestParameters.recommendBean),
                        }, initOverrides)];
                    case 3:
                        response = _a.sent();
                        return [2 /*return*/, new runtime.JSONApiResponse(response, function (jsonValue) { return (0, models_1.RecommendBeanFromJSON)(jsonValue); })];
                }
            });
        });
    };
    /**
     * 创建新的推荐
     */
    RecommendApi.prototype.recommendCreate = function (requestParameters, initOverrides) {
        return __awaiter(this, void 0, void 0, function () {
            var response;
            return __generator(this, function (_a) {
                switch (_a.label) {
                    case 0: return [4 /*yield*/, this.recommendCreateRaw(requestParameters, initOverrides)];
                    case 1:
                        response = _a.sent();
                        return [4 /*yield*/, response.value()];
                    case 2: return [2 /*return*/, _a.sent()];
                }
            });
        });
    };
    /**
     * 删除推荐
     */
    RecommendApi.prototype.recommendDeleteRaw = function (requestParameters, initOverrides) {
        return __awaiter(this, void 0, void 0, function () {
            var queryParameters, headerParameters, token, tokenString, response;
            return __generator(this, function (_a) {
                switch (_a.label) {
                    case 0:
                        if (requestParameters.app === null || requestParameters.app === undefined) {
                            throw new runtime.RequiredError('app', 'Required parameter requestParameters.app was null or undefined when calling recommendDelete.');
                        }
                        if (requestParameters.id === null || requestParameters.id === undefined) {
                            throw new runtime.RequiredError('id', 'Required parameter requestParameters.id was null or undefined when calling recommendDelete.');
                        }
                        queryParameters = {};
                        if (requestParameters.id !== undefined) {
                            queryParameters['id'] = requestParameters.id;
                        }
                        headerParameters = {};
                        if (!(this.configuration && this.configuration.accessToken)) return [3 /*break*/, 2];
                        token = this.configuration.accessToken;
                        return [4 /*yield*/, token("TokenAuth", [])];
                    case 1:
                        tokenString = _a.sent();
                        if (tokenString) {
                            headerParameters["Authorization"] = "Bearer ".concat(tokenString);
                        }
                        _a.label = 2;
                    case 2: return [4 /*yield*/, this.request({
                            path: "/recommends/{app}".replace("{".concat("app", "}"), encodeURIComponent(String(requestParameters.app))),
                            method: 'DELETE',
                            headers: headerParameters,
                            query: queryParameters,
                        }, initOverrides)];
                    case 3:
                        response = _a.sent();
                        return [2 /*return*/, new runtime.TextApiResponse(response)];
                }
            });
        });
    };
    /**
     * 删除推荐
     */
    RecommendApi.prototype.recommendDelete = function (requestParameters, initOverrides) {
        return __awaiter(this, void 0, void 0, function () {
            var response;
            return __generator(this, function (_a) {
                switch (_a.label) {
                    case 0: return [4 /*yield*/, this.recommendDeleteRaw(requestParameters, initOverrides)];
                    case 1:
                        response = _a.sent();
                        return [4 /*yield*/, response.value()];
                    case 2: return [2 /*return*/, _a.sent()];
                }
            });
        });
    };
    /**
     * 获取推荐的记录列表
     */
    RecommendApi.prototype.recommendDetailRaw = function (requestParameters, initOverrides) {
        return __awaiter(this, void 0, void 0, function () {
            var queryParameters, headerParameters, response;
            return __generator(this, function (_a) {
                switch (_a.label) {
                    case 0:
                        if (requestParameters.ident === null || requestParameters.ident === undefined) {
                            throw new runtime.RequiredError('ident', 'Required parameter requestParameters.ident was null or undefined when calling recommendDetail.');
                        }
                        queryParameters = {};
                        headerParameters = {};
                        if (requestParameters.xToken !== undefined && requestParameters.xToken !== null) {
                            headerParameters['x-token'] = String(requestParameters.xToken);
                        }
                        return [4 /*yield*/, this.request({
                                path: "/recommend/{ident}".replace("{".concat("ident", "}"), encodeURIComponent(String(requestParameters.ident))),
                                method: 'GET',
                                headers: headerParameters,
                                query: queryParameters,
                            }, initOverrides)];
                    case 1:
                        response = _a.sent();
                        return [2 /*return*/, new runtime.JSONApiResponse(response, function (jsonValue) { return (0, models_1.RecommendBeanFromJSON)(jsonValue); })];
                }
            });
        });
    };
    /**
     * 获取推荐的记录列表
     */
    RecommendApi.prototype.recommendDetail = function (requestParameters, initOverrides) {
        return __awaiter(this, void 0, void 0, function () {
            var response;
            return __generator(this, function (_a) {
                switch (_a.label) {
                    case 0: return [4 /*yield*/, this.recommendDetailRaw(requestParameters, initOverrides)];
                    case 1:
                        response = _a.sent();
                        return [4 /*yield*/, response.value()];
                    case 2: return [2 /*return*/, _a.sent()];
                }
            });
        });
    };
    /**
     * 获取推荐的记录列表
     */
    RecommendApi.prototype.recommendFetchRaw = function (requestParameters, initOverrides) {
        return __awaiter(this, void 0, void 0, function () {
            var queryParameters, headerParameters, response;
            return __generator(this, function (_a) {
                switch (_a.label) {
                    case 0:
                        if (requestParameters.ident === null || requestParameters.ident === undefined) {
                            throw new runtime.RequiredError('ident', 'Required parameter requestParameters.ident was null or undefined when calling recommendFetch.');
                        }
                        queryParameters = {};
                        if (requestParameters.condition !== undefined) {
                            queryParameters['condition'] = requestParameters.condition;
                        }
                        if (requestParameters.from !== undefined) {
                            queryParameters['from'] = requestParameters.from;
                        }
                        if (requestParameters.size !== undefined) {
                            queryParameters['size'] = requestParameters.size;
                        }
                        headerParameters = {};
                        if (requestParameters.xToken !== undefined && requestParameters.xToken !== null) {
                            headerParameters['x-token'] = String(requestParameters.xToken);
                        }
                        if (requestParameters.userid !== undefined && requestParameters.userid !== null) {
                            headerParameters['userid'] = String(requestParameters.userid);
                        }
                        return [4 /*yield*/, this.request({
                                path: "/recommend/{ident}".replace("{".concat("ident", "}"), encodeURIComponent(String(requestParameters.ident))),
                                method: 'POST',
                                headers: headerParameters,
                                query: queryParameters,
                            }, initOverrides)];
                    case 1:
                        response = _a.sent();
                        return [2 /*return*/, new runtime.JSONApiResponse(response)];
                }
            });
        });
    };
    /**
     * 获取推荐的记录列表
     */
    RecommendApi.prototype.recommendFetch = function (requestParameters, initOverrides) {
        return __awaiter(this, void 0, void 0, function () {
            var response;
            return __generator(this, function (_a) {
                switch (_a.label) {
                    case 0: return [4 /*yield*/, this.recommendFetchRaw(requestParameters, initOverrides)];
                    case 1:
                        response = _a.sent();
                        return [4 /*yield*/, response.value()];
                    case 2: return [2 /*return*/, _a.sent()];
                }
            });
        });
    };
    /**
     * 获取已定义的推荐列表
     */
    RecommendApi.prototype.recommendListRaw = function (requestParameters, initOverrides) {
        return __awaiter(this, void 0, void 0, function () {
            var queryParameters, headerParameters, token, tokenString, response;
            return __generator(this, function (_a) {
                switch (_a.label) {
                    case 0:
                        if (requestParameters.app === null || requestParameters.app === undefined) {
                            throw new runtime.RequiredError('app', 'Required parameter requestParameters.app was null or undefined when calling recommendList.');
                        }
                        queryParameters = {};
                        headerParameters = {};
                        if (!(this.configuration && this.configuration.accessToken)) return [3 /*break*/, 2];
                        token = this.configuration.accessToken;
                        return [4 /*yield*/, token("TokenAuth", [])];
                    case 1:
                        tokenString = _a.sent();
                        if (tokenString) {
                            headerParameters["Authorization"] = "Bearer ".concat(tokenString);
                        }
                        _a.label = 2;
                    case 2: return [4 /*yield*/, this.request({
                            path: "/recommends/{app}".replace("{".concat("app", "}"), encodeURIComponent(String(requestParameters.app))),
                            method: 'GET',
                            headers: headerParameters,
                            query: queryParameters,
                        }, initOverrides)];
                    case 3:
                        response = _a.sent();
                        return [2 /*return*/, new runtime.JSONApiResponse(response, function (jsonValue) { return jsonValue.map(models_1.RecommendBeanFromJSON); })];
                }
            });
        });
    };
    /**
     * 获取已定义的推荐列表
     */
    RecommendApi.prototype.recommendList = function (requestParameters, initOverrides) {
        return __awaiter(this, void 0, void 0, function () {
            var response;
            return __generator(this, function (_a) {
                switch (_a.label) {
                    case 0: return [4 /*yield*/, this.recommendListRaw(requestParameters, initOverrides)];
                    case 1:
                        response = _a.sent();
                        return [4 /*yield*/, response.value()];
                    case 2: return [2 /*return*/, _a.sent()];
                }
            });
        });
    };
    /**
     * 更新推荐信息
     */
    RecommendApi.prototype.recommendUpdateRaw = function (requestParameters, initOverrides) {
        return __awaiter(this, void 0, void 0, function () {
            var queryParameters, headerParameters, token, tokenString, response;
            return __generator(this, function (_a) {
                switch (_a.label) {
                    case 0:
                        if (requestParameters.app === null || requestParameters.app === undefined) {
                            throw new runtime.RequiredError('app', 'Required parameter requestParameters.app was null or undefined when calling recommendUpdate.');
                        }
                        if (requestParameters.recommendBean === null || requestParameters.recommendBean === undefined) {
                            throw new runtime.RequiredError('recommendBean', 'Required parameter requestParameters.recommendBean was null or undefined when calling recommendUpdate.');
                        }
                        queryParameters = {};
                        headerParameters = {};
                        headerParameters['Content-Type'] = 'application/json';
                        if (!(this.configuration && this.configuration.accessToken)) return [3 /*break*/, 2];
                        token = this.configuration.accessToken;
                        return [4 /*yield*/, token("TokenAuth", [])];
                    case 1:
                        tokenString = _a.sent();
                        if (tokenString) {
                            headerParameters["Authorization"] = "Bearer ".concat(tokenString);
                        }
                        _a.label = 2;
                    case 2: return [4 /*yield*/, this.request({
                            path: "/recommends/{app}".replace("{".concat("app", "}"), encodeURIComponent(String(requestParameters.app))),
                            method: 'PUT',
                            headers: headerParameters,
                            query: queryParameters,
                            body: (0, models_1.RecommendBeanToJSON)(requestParameters.recommendBean),
                        }, initOverrides)];
                    case 3:
                        response = _a.sent();
                        return [2 /*return*/, new runtime.TextApiResponse(response)];
                }
            });
        });
    };
    /**
     * 更新推荐信息
     */
    RecommendApi.prototype.recommendUpdate = function (requestParameters, initOverrides) {
        return __awaiter(this, void 0, void 0, function () {
            var response;
            return __generator(this, function (_a) {
                switch (_a.label) {
                    case 0: return [4 /*yield*/, this.recommendUpdateRaw(requestParameters, initOverrides)];
                    case 1:
                        response = _a.sent();
                        return [4 /*yield*/, response.value()];
                    case 2: return [2 /*return*/, _a.sent()];
                }
            });
        });
    };
    return RecommendApi;
}(runtime.BaseAPI));
exports.RecommendApi = RecommendApi;
