"use strict";
/* tslint:disable */
/* eslint-disable */
/**
 * Indexea OpenAPI
 * 这是 Indexea 搜索服务平台的 OpenAPI，用于描述平台的所有接口信息，你可以通过这个页面来了解和在线验证平台的所有接口信息。  ### Errors  本 API 使用标准的 HTTP 状态码来指示操作成功或者失败，如果失败将会在 body 中以 JSON 格式提供详细的错误信息，如下所示：  ``` {   \"error\": 404,   \"message\": \"page not found\" } ```
 *
 * The version of the OpenAPI document: 1.0.0
 * Contact: indexea.com@gmail.com
 *
 * NOTE: This class is auto generated by OpenAPI Generator (https://openapi-generator.tech).
 * https://openapi-generator.tech
 * Do not edit the class manually.
 */
Object.defineProperty(exports, "__esModule", { value: true });
exports.QueryActionBeanToJSON = exports.QueryActionBeanFromJSONTyped = exports.QueryActionBeanFromJSON = exports.instanceOfQueryActionBean = void 0;
var runtime_1 = require("../runtime");
/**
 * Check if a given object implements the QueryActionBean interface.
 */
function instanceOfQueryActionBean(value) {
    var isInstance = true;
    return isInstance;
}
exports.instanceOfQueryActionBean = instanceOfQueryActionBean;
function QueryActionBeanFromJSON(json) {
    return QueryActionBeanFromJSONTyped(json, false);
}
exports.QueryActionBeanFromJSON = QueryActionBeanFromJSON;
function QueryActionBeanFromJSONTyped(json, ignoreDiscriminator) {
    if ((json === undefined) || (json === null)) {
        return json;
    }
    return {
        'id': !(0, runtime_1.exists)(json, 'id') ? undefined : json['id'],
        'app': !(0, runtime_1.exists)(json, 'app') ? undefined : json['app'],
        'index': !(0, runtime_1.exists)(json, 'index') ? undefined : json['index'],
        'query': !(0, runtime_1.exists)(json, 'query') ? undefined : json['query'],
        'widget': !(0, runtime_1.exists)(json, 'widget') ? undefined : json['widget'],
        'recomm': !(0, runtime_1.exists)(json, 'recomm') ? undefined : json['recomm'],
        'error': !(0, runtime_1.exists)(json, 'error') ? undefined : json['error'],
        'user': !(0, runtime_1.exists)(json, 'user') ? undefined : json['user'],
        'q': !(0, runtime_1.exists)(json, 'q') ? undefined : json['q'],
        'referer': !(0, runtime_1.exists)(json, 'referer') ? undefined : json['referer'],
        'params': !(0, runtime_1.exists)(json, 'params') ? undefined : json['params'],
        'hits': !(0, runtime_1.exists)(json, 'hits') ? undefined : json['hits'],
        'took': !(0, runtime_1.exists)(json, 'took') ? undefined : json['took'],
        'ip': !(0, runtime_1.exists)(json, 'ip') ? undefined : json['ip'],
        'nation': !(0, runtime_1.exists)(json, 'nation') ? undefined : json['nation'],
        'province': !(0, runtime_1.exists)(json, 'province') ? undefined : json['province'],
        'city': !(0, runtime_1.exists)(json, 'city') ? undefined : json['city'],
        'browser': !(0, runtime_1.exists)(json, 'browser') ? undefined : json['browser'],
        'os': !(0, runtime_1.exists)(json, 'os') ? undefined : json['os'],
        'clicks': !(0, runtime_1.exists)(json, 'clicks') ? undefined : json['clicks'],
        'createdAt': !(0, runtime_1.exists)(json, 'created_at') ? undefined : (new Date(json['created_at'])),
    };
}
exports.QueryActionBeanFromJSONTyped = QueryActionBeanFromJSONTyped;
function QueryActionBeanToJSON(value) {
    if (value === undefined) {
        return undefined;
    }
    if (value === null) {
        return null;
    }
    return {
        'id': value.id,
        'app': value.app,
        'index': value.index,
        'query': value.query,
        'widget': value.widget,
        'recomm': value.recomm,
        'error': value.error,
        'user': value.user,
        'q': value.q,
        'referer': value.referer,
        'params': value.params,
        'hits': value.hits,
        'took': value.took,
        'ip': value.ip,
        'nation': value.nation,
        'province': value.province,
        'city': value.city,
        'browser': value.browser,
        'os': value.os,
        'clicks': value.clicks,
        'created_at': value.createdAt === undefined ? undefined : (value.createdAt.toISOString()),
    };
}
exports.QueryActionBeanToJSON = QueryActionBeanToJSON;
