/**
 * Indexea OpenAPI
 * 这是 Indexea 搜索服务平台的 OpenAPI，用于描述平台的所有接口信息，你可以通过这个页面来了解和在线验证平台的所有接口信息。  ### Errors  本 API 使用标准的 HTTP 状态码来指示操作成功或者失败，如果失败将会在 body 中以 JSON 格式提供详细的错误信息，如下所示：  ``` {   \"error\": 404,   \"message\": \"page not found\" } ```
 *
 * The version of the OpenAPI document: 1.0.0
 * Contact: indexea.com@gmail.com
 *
 * NOTE: This class is auto generated by OpenAPI Generator (https://openapi-generator.tech).
 * https://openapi-generator.tech
 * Do not edit the class manually.
 */
import type { IndexBean } from './IndexBean';
import type { QuerySortField } from './QuerySortField';
/**
 * 查询对象详情
 * @export
 * @interface QueryBean
 */
export interface QueryBean {
    /**
     *
     * @type {number}
     * @memberof QueryBean
     */
    id?: number;
    /**
     *
     * @type {string}
     * @memberof QueryBean
     */
    ident?: string;
    /**
     *
     * @type {number}
     * @memberof QueryBean
     */
    account?: number;
    /**
     *
     * @type {number}
     * @memberof QueryBean
     */
    app?: number;
    /**
     * 关联的索引列表
     * @type {Array<IndexBean>}
     * @memberof QueryBean
     */
    indices?: Array<IndexBean>;
    /**
     *
     * @type {string}
     * @memberof QueryBean
     */
    name?: string;
    /**
     *
     * @type {string}
     * @memberof QueryBean
     */
    intro?: string;
    /**
     *
     * @type {object}
     * @memberof QueryBean
     */
    query?: object;
    /**
     *
     * @type {object}
     * @memberof QueryBean
     */
    suggest?: object;
    /**
     *
     * @type {object}
     * @memberof QueryBean
     */
    scriptScore?: object;
    /**
     *
     * @type {object}
     * @memberof QueryBean
     */
    scriptFields?: object;
    /**
     *
     * @type {Array<QuerySortField>}
     * @memberof QueryBean
     */
    sortFields?: Array<QuerySortField>;
    /**
     *
     * @type {Date}
     * @memberof QueryBean
     */
    createdAt?: Date;
    /**
     *
     * @type {Date}
     * @memberof QueryBean
     */
    updatedAt?: Date;
    /**
     * 查询设置项
     * @type {object}
     * @memberof QueryBean
     */
    settings?: object;
    /**
     *
     * @type {number}
     * @memberof QueryBean
     */
    status?: number;
}
/**
 * Check if a given object implements the QueryBean interface.
 */
export declare function instanceOfQueryBean(value: object): boolean;
export declare function QueryBeanFromJSON(json: any): QueryBean;
export declare function QueryBeanFromJSONTyped(json: any, ignoreDiscriminator: boolean): QueryBean;
export declare function QueryBeanToJSON(value?: QueryBean | null): any;
