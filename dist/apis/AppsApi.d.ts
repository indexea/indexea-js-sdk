/**
 * Indexea OpenAPI
 * 这是 Indexea 搜索服务平台的 OpenAPI，用于描述平台的所有接口信息，你可以通过这个页面来了解和在线验证平台的所有接口信息。  ### Errors  本 API 使用标准的 HTTP 状态码来指示操作成功或者失败，如果失败将会在 body 中以 JSON 格式提供详细的错误信息，如下所示：  ``` {   \"error\": 404,   \"message\": \"page not found\" } ```
 *
 * The version of the OpenAPI document: 1.0.0
 * Contact: indexea.com@gmail.com
 *
 * NOTE: This class is auto generated by OpenAPI Generator (https://openapi-generator.tech).
 * https://openapi-generator.tech
 * Do not edit the class manually.
 */
import * as runtime from '../runtime';
import type { AppBean, AppLogsBean, BlacklistBean, CompanyBean, MentorForm, OauthAppBean, SearchEstimateResult, TokenBean, TriggerBean, TriggerLogBean } from '../models';
export interface AppAddMentorRequest {
    app: string;
    account: string;
    scopes: string;
    name?: string;
}
export interface AppBlacklistRequest {
    app: string;
}
export interface AppCreateRequest {
    name: string;
    intro?: string;
}
export interface AppCreateAccessTokenRequest {
    app: string;
    tokenBean: TokenBean;
}
export interface AppCreateOauthAppRequest {
    app: string;
    oauthAppBean: OauthAppBean;
}
export interface AppDeleteRequest {
    app: string;
}
export interface AppDeleteAccessTokenRequest {
    app: string;
    id: number;
    vcode: string;
}
export interface AppDeleteMentorRequest {
    app: string;
    account: number;
}
export interface AppDeleteOauthAppRequest {
    app: string;
    ident: string;
    vcode: string;
}
export interface AppExcelOfLogsRequest {
    app: string;
    account?: number;
    indices?: Array<number>;
    widget?: number;
    query?: number;
    type?: number;
    startDate?: Date;
    endDate?: Date;
}
export interface AppGetRequest {
    app: string;
}
export interface AppGetCompanyRequest {
    app: string;
}
export interface AppGetCompanyPicRequest {
    app: string;
    type: AppGetCompanyPicTypeEnum;
}
export interface AppListMentorsRequest {
    app: string;
    from?: number;
    size?: number;
}
export interface AppListOauthAppsRequest {
    app: string;
}
export interface AppLogsRequest {
    app: string;
    account?: number;
    indices?: Array<number>;
    widget?: number;
    query?: number;
    type?: number;
    startDate?: Date;
    endDate?: Date;
    from?: number;
    size?: number;
}
export interface AppResetAccessTokenRequest {
    app: string;
    id: number;
    vcode: string;
}
export interface AppResetOauthAppSecretRequest {
    app: string;
    ident: string;
    vcode: string;
}
export interface AppSaveBlacklistRequest {
    app: string;
    blacklistBean: BlacklistBean;
}
export interface AppSaveCompanyRequest {
    app: string;
    name?: string;
    url?: string;
    nation?: string;
    province?: string;
    city?: string;
    taxpayer?: string;
    bank?: string;
    account?: string;
    address?: string;
    tel?: string;
    license?: Blob;
    certificate?: Blob;
    postAddr?: string;
    postCode?: string;
    postName?: string;
    postTel?: string;
}
export interface AppSearchsEstimateRequest {
    app: string;
    days: number;
}
export interface AppSetTriggerRequest {
    app: string;
    triggerBean: TriggerBean;
}
export interface AppTokensRequest {
    app: string;
}
export interface AppTransferRequest {
    app: string;
    vcode: string;
    account: number;
}
export interface AppTriggerRequest {
    app: string;
}
export interface AppTriggerLogsRequest {
    app: string;
    id: number;
    size: number;
}
export interface AppUpdateRequest {
    app: string;
    name?: string;
    intro?: string;
}
export interface AppUpdateAccessTokenRequest {
    app: string;
    tokenBean: TokenBean;
}
export interface AppUpdateMentorRequest {
    app: string;
    account: number;
    name: string;
    scopes: string;
}
export interface AppUpdateMentorOptionsRequest {
    app: string;
    name?: string;
    report?: boolean;
}
export interface AppUpdateMentorReportOptionsRequest {
    app: string;
    key: string;
    type: AppUpdateMentorReportOptionsTypeEnum;
    value: string;
    vcode?: string;
}
export interface AppUpdateOauthAppRequest {
    app: string;
    oauthAppBean: OauthAppBean;
}
export interface AppUpdateOauthAppLogoRequest {
    app: string;
    ident: string;
    logo?: Blob;
}
export interface AppUpdateStatusRequest {
    app: string;
    vcode: string;
    status: number;
}
/**
 *
 */
export declare class AppsApi extends runtime.BaseAPI {
    /**
     *
     * 添加应用成员
     */
    appAddMentorRaw(requestParameters: AppAddMentorRequest, initOverrides?: RequestInit | runtime.InitOverrideFunction): Promise<runtime.ApiResponse<MentorForm>>;
    /**
     *
     * 添加应用成员
     */
    appAddMentor(requestParameters: AppAddMentorRequest, initOverrides?: RequestInit | runtime.InitOverrideFunction): Promise<MentorForm>;
    /**
     *
     * 获取黑名单信息
     */
    appBlacklistRaw(requestParameters: AppBlacklistRequest, initOverrides?: RequestInit | runtime.InitOverrideFunction): Promise<runtime.ApiResponse<BlacklistBean>>;
    /**
     *
     * 获取黑名单信息
     */
    appBlacklist(requestParameters: AppBlacklistRequest, initOverrides?: RequestInit | runtime.InitOverrideFunction): Promise<BlacklistBean>;
    /**
     *
     * 创建应用
     */
    appCreateRaw(requestParameters: AppCreateRequest, initOverrides?: RequestInit | runtime.InitOverrideFunction): Promise<runtime.ApiResponse<AppBean>>;
    /**
     *
     * 创建应用
     */
    appCreate(requestParameters: AppCreateRequest, initOverrides?: RequestInit | runtime.InitOverrideFunction): Promise<AppBean>;
    /**
     *
     * 创建 Access Token
     */
    appCreateAccessTokenRaw(requestParameters: AppCreateAccessTokenRequest, initOverrides?: RequestInit | runtime.InitOverrideFunction): Promise<runtime.ApiResponse<TokenBean>>;
    /**
     *
     * 创建 Access Token
     */
    appCreateAccessToken(requestParameters: AppCreateAccessTokenRequest, initOverrides?: RequestInit | runtime.InitOverrideFunction): Promise<TokenBean>;
    /**
     *
     * 创建第三方应用
     */
    appCreateOauthAppRaw(requestParameters: AppCreateOauthAppRequest, initOverrides?: RequestInit | runtime.InitOverrideFunction): Promise<runtime.ApiResponse<OauthAppBean>>;
    /**
     *
     * 创建第三方应用
     */
    appCreateOauthApp(requestParameters: AppCreateOauthAppRequest, initOverrides?: RequestInit | runtime.InitOverrideFunction): Promise<OauthAppBean>;
    /**
     *
     * 删除应用
     */
    appDeleteRaw(requestParameters: AppDeleteRequest, initOverrides?: RequestInit | runtime.InitOverrideFunction): Promise<runtime.ApiResponse<boolean>>;
    /**
     *
     * 删除应用
     */
    appDelete(requestParameters: AppDeleteRequest, initOverrides?: RequestInit | runtime.InitOverrideFunction): Promise<boolean>;
    /**
     *
     * 删除 Access Token
     */
    appDeleteAccessTokenRaw(requestParameters: AppDeleteAccessTokenRequest, initOverrides?: RequestInit | runtime.InitOverrideFunction): Promise<runtime.ApiResponse<boolean>>;
    /**
     *
     * 删除 Access Token
     */
    appDeleteAccessToken(requestParameters: AppDeleteAccessTokenRequest, initOverrides?: RequestInit | runtime.InitOverrideFunction): Promise<boolean>;
    /**
     *
     * 删除应用成员
     */
    appDeleteMentorRaw(requestParameters: AppDeleteMentorRequest, initOverrides?: RequestInit | runtime.InitOverrideFunction): Promise<runtime.ApiResponse<boolean>>;
    /**
     *
     * 删除应用成员
     */
    appDeleteMentor(requestParameters: AppDeleteMentorRequest, initOverrides?: RequestInit | runtime.InitOverrideFunction): Promise<boolean>;
    /**
     *
     * 删除第三方应用
     */
    appDeleteOauthAppRaw(requestParameters: AppDeleteOauthAppRequest, initOverrides?: RequestInit | runtime.InitOverrideFunction): Promise<runtime.ApiResponse<boolean>>;
    /**
     *
     * 删除第三方应用
     */
    appDeleteOauthApp(requestParameters: AppDeleteOauthAppRequest, initOverrides?: RequestInit | runtime.InitOverrideFunction): Promise<boolean>;
    /**
     * 导出应用日志到 Excel
     */
    appExcelOfLogsRaw(requestParameters: AppExcelOfLogsRequest, initOverrides?: RequestInit | runtime.InitOverrideFunction): Promise<runtime.ApiResponse<Blob>>;
    /**
     * 导出应用日志到 Excel
     */
    appExcelOfLogs(requestParameters: AppExcelOfLogsRequest, initOverrides?: RequestInit | runtime.InitOverrideFunction): Promise<Blob>;
    /**
     *
     * 获取应用详情
     */
    appGetRaw(requestParameters: AppGetRequest, initOverrides?: RequestInit | runtime.InitOverrideFunction): Promise<runtime.ApiResponse<AppBean>>;
    /**
     *
     * 获取应用详情
     */
    appGet(requestParameters: AppGetRequest, initOverrides?: RequestInit | runtime.InitOverrideFunction): Promise<AppBean>;
    /**
     *
     * 获取应用填写的公司信息
     */
    appGetCompanyRaw(requestParameters: AppGetCompanyRequest, initOverrides?: RequestInit | runtime.InitOverrideFunction): Promise<runtime.ApiResponse<CompanyBean>>;
    /**
     *
     * 获取应用填写的公司信息
     */
    appGetCompany(requestParameters: AppGetCompanyRequest, initOverrides?: RequestInit | runtime.InitOverrideFunction): Promise<CompanyBean>;
    /**
     *
     * 获取公司营业执照或者一般纳税人证明
     */
    appGetCompanyPicRaw(requestParameters: AppGetCompanyPicRequest, initOverrides?: RequestInit | runtime.InitOverrideFunction): Promise<runtime.ApiResponse<Blob>>;
    /**
     *
     * 获取公司营业执照或者一般纳税人证明
     */
    appGetCompanyPic(requestParameters: AppGetCompanyPicRequest, initOverrides?: RequestInit | runtime.InitOverrideFunction): Promise<Blob>;
    /**
     *
     * 获取应用列表
     */
    appListRaw(initOverrides?: RequestInit | runtime.InitOverrideFunction): Promise<runtime.ApiResponse<Array<AppBean>>>;
    /**
     *
     * 获取应用列表
     */
    appList(initOverrides?: RequestInit | runtime.InitOverrideFunction): Promise<Array<AppBean>>;
    /**
     *
     * 获取应用成员列表
     */
    appListMentorsRaw(requestParameters: AppListMentorsRequest, initOverrides?: RequestInit | runtime.InitOverrideFunction): Promise<runtime.ApiResponse<MentorForm>>;
    /**
     *
     * 获取应用成员列表
     */
    appListMentors(requestParameters: AppListMentorsRequest, initOverrides?: RequestInit | runtime.InitOverrideFunction): Promise<MentorForm>;
    /**
     *
     * 获取第三方应用列表
     */
    appListOauthAppsRaw(requestParameters: AppListOauthAppsRequest, initOverrides?: RequestInit | runtime.InitOverrideFunction): Promise<runtime.ApiResponse<Array<OauthAppBean>>>;
    /**
     *
     * 获取第三方应用列表
     */
    appListOauthApps(requestParameters: AppListOauthAppsRequest, initOverrides?: RequestInit | runtime.InitOverrideFunction): Promise<Array<OauthAppBean>>;
    /**
     *
     * 获取应用的日志列表
     */
    appLogsRaw(requestParameters: AppLogsRequest, initOverrides?: RequestInit | runtime.InitOverrideFunction): Promise<runtime.ApiResponse<AppLogsBean>>;
    /**
     *
     * 获取应用的日志列表
     */
    appLogs(requestParameters: AppLogsRequest, initOverrides?: RequestInit | runtime.InitOverrideFunction): Promise<AppLogsBean>;
    /**
     *
     * 重置 Access Token
     */
    appResetAccessTokenRaw(requestParameters: AppResetAccessTokenRequest, initOverrides?: RequestInit | runtime.InitOverrideFunction): Promise<runtime.ApiResponse<TokenBean>>;
    /**
     *
     * 重置 Access Token
     */
    appResetAccessToken(requestParameters: AppResetAccessTokenRequest, initOverrides?: RequestInit | runtime.InitOverrideFunction): Promise<TokenBean>;
    /**
     *
     * 重新生成三方应用的密钥
     */
    appResetOauthAppSecretRaw(requestParameters: AppResetOauthAppSecretRequest, initOverrides?: RequestInit | runtime.InitOverrideFunction): Promise<runtime.ApiResponse<OauthAppBean>>;
    /**
     *
     * 重新生成三方应用的密钥
     */
    appResetOauthAppSecret(requestParameters: AppResetOauthAppSecretRequest, initOverrides?: RequestInit | runtime.InitOverrideFunction): Promise<OauthAppBean>;
    /**
     *
     * 修改应用的黑名单信息
     */
    appSaveBlacklistRaw(requestParameters: AppSaveBlacklistRequest, initOverrides?: RequestInit | runtime.InitOverrideFunction): Promise<runtime.ApiResponse<boolean>>;
    /**
     *
     * 修改应用的黑名单信息
     */
    appSaveBlacklist(requestParameters: AppSaveBlacklistRequest, initOverrides?: RequestInit | runtime.InitOverrideFunction): Promise<boolean>;
    /**
     *
     * 修改应用的公司信息
     */
    appSaveCompanyRaw(requestParameters: AppSaveCompanyRequest, initOverrides?: RequestInit | runtime.InitOverrideFunction): Promise<runtime.ApiResponse<CompanyBean>>;
    /**
     *
     * 修改应用的公司信息
     */
    appSaveCompany(requestParameters: AppSaveCompanyRequest, initOverrides?: RequestInit | runtime.InitOverrideFunction): Promise<CompanyBean>;
    /**
     * 获取搜索流量包使用配额信息
     */
    appSearchsEstimateRaw(requestParameters: AppSearchsEstimateRequest, initOverrides?: RequestInit | runtime.InitOverrideFunction): Promise<runtime.ApiResponse<SearchEstimateResult>>;
    /**
     * 获取搜索流量包使用配额信息
     */
    appSearchsEstimate(requestParameters: AppSearchsEstimateRequest, initOverrides?: RequestInit | runtime.InitOverrideFunction): Promise<SearchEstimateResult>;
    /**
     *
     * 修改应用的触发器信息
     */
    appSetTriggerRaw(requestParameters: AppSetTriggerRequest, initOverrides?: RequestInit | runtime.InitOverrideFunction): Promise<runtime.ApiResponse<boolean>>;
    /**
     *
     * 修改应用的触发器信息
     */
    appSetTrigger(requestParameters: AppSetTriggerRequest, initOverrides?: RequestInit | runtime.InitOverrideFunction): Promise<boolean>;
    /**
     *
     * 获取 Access Token 列表
     */
    appTokensRaw(requestParameters: AppTokensRequest, initOverrides?: RequestInit | runtime.InitOverrideFunction): Promise<runtime.ApiResponse<Array<TokenBean>>>;
    /**
     *
     * 获取 Access Token 列表
     */
    appTokens(requestParameters: AppTokensRequest, initOverrides?: RequestInit | runtime.InitOverrideFunction): Promise<Array<TokenBean>>;
    /**
     *
     * 转让应用给他人
     */
    appTransferRaw(requestParameters: AppTransferRequest, initOverrides?: RequestInit | runtime.InitOverrideFunction): Promise<runtime.ApiResponse<boolean>>;
    /**
     *
     * 转让应用给他人
     */
    appTransfer(requestParameters: AppTransferRequest, initOverrides?: RequestInit | runtime.InitOverrideFunction): Promise<boolean>;
    /**
     * 获取应用触发器详情
     */
    appTriggerRaw(requestParameters: AppTriggerRequest, initOverrides?: RequestInit | runtime.InitOverrideFunction): Promise<runtime.ApiResponse<TriggerBean>>;
    /**
     * 获取应用触发器详情
     */
    appTrigger(requestParameters: AppTriggerRequest, initOverrides?: RequestInit | runtime.InitOverrideFunction): Promise<TriggerBean>;
    /**
     * 获取应用触发日志列表
     */
    appTriggerLogsRaw(requestParameters: AppTriggerLogsRequest, initOverrides?: RequestInit | runtime.InitOverrideFunction): Promise<runtime.ApiResponse<Array<TriggerLogBean>>>;
    /**
     * 获取应用触发日志列表
     */
    appTriggerLogs(requestParameters: AppTriggerLogsRequest, initOverrides?: RequestInit | runtime.InitOverrideFunction): Promise<Array<TriggerLogBean>>;
    /**
     *
     * 修改应用的基本信息
     */
    appUpdateRaw(requestParameters: AppUpdateRequest, initOverrides?: RequestInit | runtime.InitOverrideFunction): Promise<runtime.ApiResponse<AppBean>>;
    /**
     *
     * 修改应用的基本信息
     */
    appUpdate(requestParameters: AppUpdateRequest, initOverrides?: RequestInit | runtime.InitOverrideFunction): Promise<AppBean>;
    /**
     *
     * 修改 Access Token
     */
    appUpdateAccessTokenRaw(requestParameters: AppUpdateAccessTokenRequest, initOverrides?: RequestInit | runtime.InitOverrideFunction): Promise<runtime.ApiResponse<TokenBean>>;
    /**
     *
     * 修改 Access Token
     */
    appUpdateAccessToken(requestParameters: AppUpdateAccessTokenRequest, initOverrides?: RequestInit | runtime.InitOverrideFunction): Promise<TokenBean>;
    /**
     *
     * 修改成员备注和权限
     */
    appUpdateMentorRaw(requestParameters: AppUpdateMentorRequest, initOverrides?: RequestInit | runtime.InitOverrideFunction): Promise<runtime.ApiResponse<boolean>>;
    /**
     *
     * 修改成员备注和权限
     */
    appUpdateMentor(requestParameters: AppUpdateMentorRequest, initOverrides?: RequestInit | runtime.InitOverrideFunction): Promise<boolean>;
    /**
     *
     * 修改应用成员自身的设置（包括应用名备注，是否接收报告等）
     */
    appUpdateMentorOptionsRaw(requestParameters: AppUpdateMentorOptionsRequest, initOverrides?: RequestInit | runtime.InitOverrideFunction): Promise<runtime.ApiResponse<boolean>>;
    /**
     *
     * 修改应用成员自身的设置（包括应用名备注，是否接收报告等）
     */
    appUpdateMentorOptions(requestParameters: AppUpdateMentorOptionsRequest, initOverrides?: RequestInit | runtime.InitOverrideFunction): Promise<boolean>;
    /**
     *
     * 修改应用成员自身的通知设置
     */
    appUpdateMentorReportOptionsRaw(requestParameters: AppUpdateMentorReportOptionsRequest, initOverrides?: RequestInit | runtime.InitOverrideFunction): Promise<runtime.ApiResponse<boolean>>;
    /**
     *
     * 修改应用成员自身的通知设置
     */
    appUpdateMentorReportOptions(requestParameters: AppUpdateMentorReportOptionsRequest, initOverrides?: RequestInit | runtime.InitOverrideFunction): Promise<boolean>;
    /**
     *
     * 修改第三方应用信息
     */
    appUpdateOauthAppRaw(requestParameters: AppUpdateOauthAppRequest, initOverrides?: RequestInit | runtime.InitOverrideFunction): Promise<runtime.ApiResponse<OauthAppBean>>;
    /**
     *
     * 修改第三方应用信息
     */
    appUpdateOauthApp(requestParameters: AppUpdateOauthAppRequest, initOverrides?: RequestInit | runtime.InitOverrideFunction): Promise<OauthAppBean>;
    /**
     *
     * 修改三方应用图标
     */
    appUpdateOauthAppLogoRaw(requestParameters: AppUpdateOauthAppLogoRequest, initOverrides?: RequestInit | runtime.InitOverrideFunction): Promise<runtime.ApiResponse<OauthAppBean>>;
    /**
     *
     * 修改三方应用图标
     */
    appUpdateOauthAppLogo(requestParameters: AppUpdateOauthAppLogoRequest, initOverrides?: RequestInit | runtime.InitOverrideFunction): Promise<OauthAppBean>;
    /**
     *
     * 修改应用的状态
     */
    appUpdateStatusRaw(requestParameters: AppUpdateStatusRequest, initOverrides?: RequestInit | runtime.InitOverrideFunction): Promise<runtime.ApiResponse<boolean>>;
    /**
     *
     * 修改应用的状态
     */
    appUpdateStatus(requestParameters: AppUpdateStatusRequest, initOverrides?: RequestInit | runtime.InitOverrideFunction): Promise<boolean>;
}
/**
 * @export
 */
export declare const AppGetCompanyPicTypeEnum: {
    readonly License: "license";
    readonly Certificate: "certificate";
};
export type AppGetCompanyPicTypeEnum = typeof AppGetCompanyPicTypeEnum[keyof typeof AppGetCompanyPicTypeEnum];
/**
 * @export
 */
export declare const AppUpdateMentorReportOptionsTypeEnum: {
    readonly Int: "int";
    readonly Bool: "bool";
    readonly String: "string";
};
export type AppUpdateMentorReportOptionsTypeEnum = typeof AppUpdateMentorReportOptionsTypeEnum[keyof typeof AppUpdateMentorReportOptionsTypeEnum];
