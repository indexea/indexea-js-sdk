"use strict";
/* tslint:disable */
/* eslint-disable */
/**
 * Indexea OpenAPI
 * 这是 Indexea 搜索服务平台的 OpenAPI，用于描述平台的所有接口信息，你可以通过这个页面来了解和在线验证平台的所有接口信息。  ### Errors  本 API 使用标准的 HTTP 状态码来指示操作成功或者失败，如果失败将会在 body 中以 JSON 格式提供详细的错误信息，如下所示：  ``` {   \"error\": 404,   \"message\": \"page not found\" } ```
 *
 * The version of the OpenAPI document: 1.0.0
 * Contact: indexea.com@gmail.com
 *
 * NOTE: This class is auto generated by OpenAPI Generator (https://openapi-generator.tech).
 * https://openapi-generator.tech
 * Do not edit the class manually.
 */
Object.defineProperty(exports, "__esModule", { value: true });
exports.RecommendBeanToJSON = exports.RecommendBeanFromJSONTyped = exports.RecommendBeanFromJSON = exports.instanceOfRecommendBean = void 0;
var runtime_1 = require("../runtime");
/**
 * Check if a given object implements the RecommendBean interface.
 */
function instanceOfRecommendBean(value) {
    var isInstance = true;
    return isInstance;
}
exports.instanceOfRecommendBean = instanceOfRecommendBean;
function RecommendBeanFromJSON(json) {
    return RecommendBeanFromJSONTyped(json, false);
}
exports.RecommendBeanFromJSON = RecommendBeanFromJSON;
function RecommendBeanFromJSONTyped(json, ignoreDiscriminator) {
    if ((json === undefined) || (json === null)) {
        return json;
    }
    return {
        'id': !(0, runtime_1.exists)(json, 'id') ? undefined : json['id'],
        'ident': !(0, runtime_1.exists)(json, 'ident') ? undefined : json['ident'],
        'app': !(0, runtime_1.exists)(json, 'app') ? undefined : json['app'],
        'account': !(0, runtime_1.exists)(json, 'account') ? undefined : json['account'],
        'index': !(0, runtime_1.exists)(json, 'index') ? undefined : json['index'],
        'name': !(0, runtime_1.exists)(json, 'name') ? undefined : json['name'],
        'intro': !(0, runtime_1.exists)(json, 'intro') ? undefined : json['intro'],
        'type': !(0, runtime_1.exists)(json, 'type') ? undefined : json['type'],
        'settings': !(0, runtime_1.exists)(json, 'settings') ? undefined : json['settings'],
        'status': !(0, runtime_1.exists)(json, 'status') ? undefined : json['status'],
        'createdAt': !(0, runtime_1.exists)(json, 'created_at') ? undefined : (new Date(json['created_at'])),
        'updatedAt': !(0, runtime_1.exists)(json, 'updated_at') ? undefined : (new Date(json['updated_at'])),
    };
}
exports.RecommendBeanFromJSONTyped = RecommendBeanFromJSONTyped;
function RecommendBeanToJSON(value) {
    if (value === undefined) {
        return undefined;
    }
    if (value === null) {
        return null;
    }
    return {
        'id': value.id,
        'ident': value.ident,
        'app': value.app,
        'account': value.account,
        'index': value.index,
        'name': value.name,
        'intro': value.intro,
        'type': value.type,
        'settings': value.settings,
        'status': value.status,
        'created_at': value.createdAt === undefined ? undefined : (value.createdAt.toISOString()),
        'updated_at': value.updatedAt === undefined ? undefined : (value.updatedAt.toISOString()),
    };
}
exports.RecommendBeanToJSON = RecommendBeanToJSON;
