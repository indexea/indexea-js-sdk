/* tslint:disable */
/* eslint-disable */
/**
 * Indexea OpenAPI
 * 这是 Indexea 搜索服务平台的 OpenAPI，用于描述平台的所有接口信息，你可以通过这个页面来了解和在线验证平台的所有接口信息。  ### Errors  本 API 使用标准的 HTTP 状态码来指示操作成功或者失败，如果失败将会在 body 中以 JSON 格式提供详细的错误信息，如下所示：  ``` {   \"error\": 404,   \"message\": \"page not found\" } ``` 
 *
 * The version of the OpenAPI document: 1.0.0
 * Contact: indexea.com@gmail.com
 *
 * NOTE: This class is auto generated by OpenAPI Generator (https://openapi-generator.tech).
 * https://openapi-generator.tech
 * Do not edit the class manually.
 */


import * as runtime from '../runtime';
import type {
  ContactForm,
  PayResult,
  PaymentInvoice,
  PaymentOrder,
  PaymentService,
} from '../models';
import {
    ContactFormFromJSON,
    ContactFormToJSON,
    PayResultFromJSON,
    PayResultToJSON,
    PaymentInvoiceFromJSON,
    PaymentInvoiceToJSON,
    PaymentOrderFromJSON,
    PaymentOrderToJSON,
    PaymentServiceFromJSON,
    PaymentServiceToJSON,
} from '../models';

export interface PaymentApplyInvoiceRequest {
    app: string;
    type: number;
    requestBody: Array<string>;
}

export interface PaymentBeginPayRequest {
    app: string;
    ident: string;
    type: PaymentBeginPayTypeEnum;
}

export interface PaymentBuyRequest {
    app: string;
    paymentService: PaymentService;
}

export interface PaymentDeleteInvoiceRequest {
    app: string;
    id: number;
}

export interface PaymentDeleteOrderRequest {
    app: string;
    ident: string;
}

export interface PaymentInvoicesRequest {
    app: string;
}

export interface PaymentOrderRequest {
    app: string;
    ident: string;
}

export interface PaymentOrdersRequest {
    app: string;
}

export interface PaymentOrdersWithoutInvoiceRequest {
    app: string;
}

export interface PaymentPriceRequest {
    app: string;
    service: PaymentService;
}

export interface PaymentReceiptRequest {
    app: string;
    ident: string;
    id: number;
}

export interface PaymentRequestContactRequest {
    app: string;
    contactForm: ContactForm;
}

export interface PaymentUploadReceiptRequest {
    app: string;
    ident: string;
    receipt?: Blob;
}

/**
 * 
 */
export class PaymentApi extends runtime.BaseAPI {

    /**
     * 接受支付宝的支付结果
     */
    async paymentAlipayRaw(initOverrides?: RequestInit | runtime.InitOverrideFunction): Promise<runtime.ApiResponse<void>> {
        const queryParameters: any = {};

        const headerParameters: runtime.HTTPHeaders = {};

        const response = await this.request({
            path: `/payment/alipay`,
            method: 'POST',
            headers: headerParameters,
            query: queryParameters,
        }, initOverrides);

        return new runtime.VoidApiResponse(response);
    }

    /**
     * 接受支付宝的支付结果
     */
    async paymentAlipay(initOverrides?: RequestInit | runtime.InitOverrideFunction): Promise<void> {
        await this.paymentAlipayRaw(initOverrides);
    }

    /**
     * 支付宝平台支付完毕后调整到该接口
     */
    async paymentAlipayReturnRaw(initOverrides?: RequestInit | runtime.InitOverrideFunction): Promise<runtime.ApiResponse<void>> {
        const queryParameters: any = {};

        const headerParameters: runtime.HTTPHeaders = {};

        const response = await this.request({
            path: `/payment/alipay`,
            method: 'GET',
            headers: headerParameters,
            query: queryParameters,
        }, initOverrides);

        return new runtime.VoidApiResponse(response);
    }

    /**
     * 支付宝平台支付完毕后调整到该接口
     */
    async paymentAlipayReturn(initOverrides?: RequestInit | runtime.InitOverrideFunction): Promise<void> {
        await this.paymentAlipayReturnRaw(initOverrides);
    }

    /**
     * 申请发票
     */
    async paymentApplyInvoiceRaw(requestParameters: PaymentApplyInvoiceRequest, initOverrides?: RequestInit | runtime.InitOverrideFunction): Promise<runtime.ApiResponse<PaymentInvoice>> {
        if (requestParameters.app === null || requestParameters.app === undefined) {
            throw new runtime.RequiredError('app','Required parameter requestParameters.app was null or undefined when calling paymentApplyInvoice.');
        }

        if (requestParameters.type === null || requestParameters.type === undefined) {
            throw new runtime.RequiredError('type','Required parameter requestParameters.type was null or undefined when calling paymentApplyInvoice.');
        }

        if (requestParameters.requestBody === null || requestParameters.requestBody === undefined) {
            throw new runtime.RequiredError('requestBody','Required parameter requestParameters.requestBody was null or undefined when calling paymentApplyInvoice.');
        }

        const queryParameters: any = {};

        if (requestParameters.type !== undefined) {
            queryParameters['type'] = requestParameters.type;
        }

        const headerParameters: runtime.HTTPHeaders = {};

        headerParameters['Content-Type'] = 'application/json';

        if (this.configuration && this.configuration.accessToken) {
            const token = this.configuration.accessToken;
            const tokenString = await token("TokenAuth", []);

            if (tokenString) {
                headerParameters["Authorization"] = `Bearer ${tokenString}`;
            }
        }
        const response = await this.request({
            path: `/payment/{app}/invoices`.replace(`{${"app"}}`, encodeURIComponent(String(requestParameters.app))),
            method: 'POST',
            headers: headerParameters,
            query: queryParameters,
            body: requestParameters.requestBody,
        }, initOverrides);

        return new runtime.JSONApiResponse(response, (jsonValue) => PaymentInvoiceFromJSON(jsonValue));
    }

    /**
     * 申请发票
     */
    async paymentApplyInvoice(requestParameters: PaymentApplyInvoiceRequest, initOverrides?: RequestInit | runtime.InitOverrideFunction): Promise<PaymentInvoice> {
        const response = await this.paymentApplyInvoiceRaw(requestParameters, initOverrides);
        return await response.value();
    }

    /**
     * 选择支付方式，开始支付
     * 支付订单
     */
    async paymentBeginPayRaw(requestParameters: PaymentBeginPayRequest, initOverrides?: RequestInit | runtime.InitOverrideFunction): Promise<runtime.ApiResponse<PayResult>> {
        if (requestParameters.app === null || requestParameters.app === undefined) {
            throw new runtime.RequiredError('app','Required parameter requestParameters.app was null or undefined when calling paymentBeginPay.');
        }

        if (requestParameters.ident === null || requestParameters.ident === undefined) {
            throw new runtime.RequiredError('ident','Required parameter requestParameters.ident was null or undefined when calling paymentBeginPay.');
        }

        if (requestParameters.type === null || requestParameters.type === undefined) {
            throw new runtime.RequiredError('type','Required parameter requestParameters.type was null or undefined when calling paymentBeginPay.');
        }

        const queryParameters: any = {};

        if (requestParameters.type !== undefined) {
            queryParameters['type'] = requestParameters.type;
        }

        const headerParameters: runtime.HTTPHeaders = {};

        if (this.configuration && this.configuration.accessToken) {
            const token = this.configuration.accessToken;
            const tokenString = await token("TokenAuth", []);

            if (tokenString) {
                headerParameters["Authorization"] = `Bearer ${tokenString}`;
            }
        }
        const response = await this.request({
            path: `/payment/{app}/orders/{ident}`.replace(`{${"app"}}`, encodeURIComponent(String(requestParameters.app))).replace(`{${"ident"}}`, encodeURIComponent(String(requestParameters.ident))),
            method: 'POST',
            headers: headerParameters,
            query: queryParameters,
        }, initOverrides);

        return new runtime.JSONApiResponse(response, (jsonValue) => PayResultFromJSON(jsonValue));
    }

    /**
     * 选择支付方式，开始支付
     * 支付订单
     */
    async paymentBeginPay(requestParameters: PaymentBeginPayRequest, initOverrides?: RequestInit | runtime.InitOverrideFunction): Promise<PayResult> {
        const response = await this.paymentBeginPayRaw(requestParameters, initOverrides);
        return await response.value();
    }

    /**
     * 
     * 下单购买
     */
    async paymentBuyRaw(requestParameters: PaymentBuyRequest, initOverrides?: RequestInit | runtime.InitOverrideFunction): Promise<runtime.ApiResponse<PaymentOrder>> {
        if (requestParameters.app === null || requestParameters.app === undefined) {
            throw new runtime.RequiredError('app','Required parameter requestParameters.app was null or undefined when calling paymentBuy.');
        }

        if (requestParameters.paymentService === null || requestParameters.paymentService === undefined) {
            throw new runtime.RequiredError('paymentService','Required parameter requestParameters.paymentService was null or undefined when calling paymentBuy.');
        }

        const queryParameters: any = {};

        const headerParameters: runtime.HTTPHeaders = {};

        headerParameters['Content-Type'] = 'application/json';

        if (this.configuration && this.configuration.accessToken) {
            const token = this.configuration.accessToken;
            const tokenString = await token("TokenAuth", []);

            if (tokenString) {
                headerParameters["Authorization"] = `Bearer ${tokenString}`;
            }
        }
        const response = await this.request({
            path: `/payment/{app}/orders`.replace(`{${"app"}}`, encodeURIComponent(String(requestParameters.app))),
            method: 'PUT',
            headers: headerParameters,
            query: queryParameters,
            body: PaymentServiceToJSON(requestParameters.paymentService),
        }, initOverrides);

        return new runtime.JSONApiResponse(response, (jsonValue) => PaymentOrderFromJSON(jsonValue));
    }

    /**
     * 
     * 下单购买
     */
    async paymentBuy(requestParameters: PaymentBuyRequest, initOverrides?: RequestInit | runtime.InitOverrideFunction): Promise<PaymentOrder> {
        const response = await this.paymentBuyRaw(requestParameters, initOverrides);
        return await response.value();
    }

    /**
     * 删除发票
     */
    async paymentDeleteInvoiceRaw(requestParameters: PaymentDeleteInvoiceRequest, initOverrides?: RequestInit | runtime.InitOverrideFunction): Promise<runtime.ApiResponse<boolean>> {
        if (requestParameters.app === null || requestParameters.app === undefined) {
            throw new runtime.RequiredError('app','Required parameter requestParameters.app was null or undefined when calling paymentDeleteInvoice.');
        }

        if (requestParameters.id === null || requestParameters.id === undefined) {
            throw new runtime.RequiredError('id','Required parameter requestParameters.id was null or undefined when calling paymentDeleteInvoice.');
        }

        const queryParameters: any = {};

        if (requestParameters.id !== undefined) {
            queryParameters['id'] = requestParameters.id;
        }

        const headerParameters: runtime.HTTPHeaders = {};

        if (this.configuration && this.configuration.accessToken) {
            const token = this.configuration.accessToken;
            const tokenString = await token("TokenAuth", []);

            if (tokenString) {
                headerParameters["Authorization"] = `Bearer ${tokenString}`;
            }
        }
        const response = await this.request({
            path: `/payment/{app}/invoices`.replace(`{${"app"}}`, encodeURIComponent(String(requestParameters.app))),
            method: 'DELETE',
            headers: headerParameters,
            query: queryParameters,
        }, initOverrides);

        return new runtime.TextApiResponse(response) as any;
    }

    /**
     * 删除发票
     */
    async paymentDeleteInvoice(requestParameters: PaymentDeleteInvoiceRequest, initOverrides?: RequestInit | runtime.InitOverrideFunction): Promise<boolean> {
        const response = await this.paymentDeleteInvoiceRaw(requestParameters, initOverrides);
        return await response.value();
    }

    /**
     * 取消订单
     */
    async paymentDeleteOrderRaw(requestParameters: PaymentDeleteOrderRequest, initOverrides?: RequestInit | runtime.InitOverrideFunction): Promise<runtime.ApiResponse<boolean>> {
        if (requestParameters.app === null || requestParameters.app === undefined) {
            throw new runtime.RequiredError('app','Required parameter requestParameters.app was null or undefined when calling paymentDeleteOrder.');
        }

        if (requestParameters.ident === null || requestParameters.ident === undefined) {
            throw new runtime.RequiredError('ident','Required parameter requestParameters.ident was null or undefined when calling paymentDeleteOrder.');
        }

        const queryParameters: any = {};

        const headerParameters: runtime.HTTPHeaders = {};

        if (this.configuration && this.configuration.accessToken) {
            const token = this.configuration.accessToken;
            const tokenString = await token("TokenAuth", []);

            if (tokenString) {
                headerParameters["Authorization"] = `Bearer ${tokenString}`;
            }
        }
        const response = await this.request({
            path: `/payment/{app}/orders/{ident}`.replace(`{${"app"}}`, encodeURIComponent(String(requestParameters.app))).replace(`{${"ident"}}`, encodeURIComponent(String(requestParameters.ident))),
            method: 'DELETE',
            headers: headerParameters,
            query: queryParameters,
        }, initOverrides);

        return new runtime.TextApiResponse(response) as any;
    }

    /**
     * 取消订单
     */
    async paymentDeleteOrder(requestParameters: PaymentDeleteOrderRequest, initOverrides?: RequestInit | runtime.InitOverrideFunction): Promise<boolean> {
        const response = await this.paymentDeleteOrderRaw(requestParameters, initOverrides);
        return await response.value();
    }

    /**
     * 获取发票列表
     */
    async paymentInvoicesRaw(requestParameters: PaymentInvoicesRequest, initOverrides?: RequestInit | runtime.InitOverrideFunction): Promise<runtime.ApiResponse<Array<PaymentInvoice>>> {
        if (requestParameters.app === null || requestParameters.app === undefined) {
            throw new runtime.RequiredError('app','Required parameter requestParameters.app was null or undefined when calling paymentInvoices.');
        }

        const queryParameters: any = {};

        const headerParameters: runtime.HTTPHeaders = {};

        if (this.configuration && this.configuration.accessToken) {
            const token = this.configuration.accessToken;
            const tokenString = await token("TokenAuth", []);

            if (tokenString) {
                headerParameters["Authorization"] = `Bearer ${tokenString}`;
            }
        }
        const response = await this.request({
            path: `/payment/{app}/invoices`.replace(`{${"app"}}`, encodeURIComponent(String(requestParameters.app))),
            method: 'GET',
            headers: headerParameters,
            query: queryParameters,
        }, initOverrides);

        return new runtime.JSONApiResponse(response, (jsonValue) => jsonValue.map(PaymentInvoiceFromJSON));
    }

    /**
     * 获取发票列表
     */
    async paymentInvoices(requestParameters: PaymentInvoicesRequest, initOverrides?: RequestInit | runtime.InitOverrideFunction): Promise<Array<PaymentInvoice>> {
        const response = await this.paymentInvoicesRaw(requestParameters, initOverrides);
        return await response.value();
    }

    /**
     * 获取订单详情
     * 获取订单详情
     */
    async paymentOrderRaw(requestParameters: PaymentOrderRequest, initOverrides?: RequestInit | runtime.InitOverrideFunction): Promise<runtime.ApiResponse<PaymentOrder>> {
        if (requestParameters.app === null || requestParameters.app === undefined) {
            throw new runtime.RequiredError('app','Required parameter requestParameters.app was null or undefined when calling paymentOrder.');
        }

        if (requestParameters.ident === null || requestParameters.ident === undefined) {
            throw new runtime.RequiredError('ident','Required parameter requestParameters.ident was null or undefined when calling paymentOrder.');
        }

        const queryParameters: any = {};

        const headerParameters: runtime.HTTPHeaders = {};

        if (this.configuration && this.configuration.accessToken) {
            const token = this.configuration.accessToken;
            const tokenString = await token("TokenAuth", []);

            if (tokenString) {
                headerParameters["Authorization"] = `Bearer ${tokenString}`;
            }
        }
        const response = await this.request({
            path: `/payment/{app}/orders/{ident}`.replace(`{${"app"}}`, encodeURIComponent(String(requestParameters.app))).replace(`{${"ident"}}`, encodeURIComponent(String(requestParameters.ident))),
            method: 'GET',
            headers: headerParameters,
            query: queryParameters,
        }, initOverrides);

        return new runtime.JSONApiResponse(response, (jsonValue) => PaymentOrderFromJSON(jsonValue));
    }

    /**
     * 获取订单详情
     * 获取订单详情
     */
    async paymentOrder(requestParameters: PaymentOrderRequest, initOverrides?: RequestInit | runtime.InitOverrideFunction): Promise<PaymentOrder> {
        const response = await this.paymentOrderRaw(requestParameters, initOverrides);
        return await response.value();
    }

    /**
     * 获取应用的订单列表
     * 订单列表
     */
    async paymentOrdersRaw(requestParameters: PaymentOrdersRequest, initOverrides?: RequestInit | runtime.InitOverrideFunction): Promise<runtime.ApiResponse<Array<PaymentOrder>>> {
        if (requestParameters.app === null || requestParameters.app === undefined) {
            throw new runtime.RequiredError('app','Required parameter requestParameters.app was null or undefined when calling paymentOrders.');
        }

        const queryParameters: any = {};

        const headerParameters: runtime.HTTPHeaders = {};

        if (this.configuration && this.configuration.accessToken) {
            const token = this.configuration.accessToken;
            const tokenString = await token("TokenAuth", []);

            if (tokenString) {
                headerParameters["Authorization"] = `Bearer ${tokenString}`;
            }
        }
        const response = await this.request({
            path: `/payment/{app}/orders`.replace(`{${"app"}}`, encodeURIComponent(String(requestParameters.app))),
            method: 'POST',
            headers: headerParameters,
            query: queryParameters,
        }, initOverrides);

        return new runtime.JSONApiResponse(response, (jsonValue) => jsonValue.map(PaymentOrderFromJSON));
    }

    /**
     * 获取应用的订单列表
     * 订单列表
     */
    async paymentOrders(requestParameters: PaymentOrdersRequest, initOverrides?: RequestInit | runtime.InitOverrideFunction): Promise<Array<PaymentOrder>> {
        const response = await this.paymentOrdersRaw(requestParameters, initOverrides);
        return await response.value();
    }

    /**
     * 获取未曾开票的订单列表
     */
    async paymentOrdersWithoutInvoiceRaw(requestParameters: PaymentOrdersWithoutInvoiceRequest, initOverrides?: RequestInit | runtime.InitOverrideFunction): Promise<runtime.ApiResponse<Array<PaymentOrder>>> {
        if (requestParameters.app === null || requestParameters.app === undefined) {
            throw new runtime.RequiredError('app','Required parameter requestParameters.app was null or undefined when calling paymentOrdersWithoutInvoice.');
        }

        const queryParameters: any = {};

        const headerParameters: runtime.HTTPHeaders = {};

        if (this.configuration && this.configuration.accessToken) {
            const token = this.configuration.accessToken;
            const tokenString = await token("TokenAuth", []);

            if (tokenString) {
                headerParameters["Authorization"] = `Bearer ${tokenString}`;
            }
        }
        const response = await this.request({
            path: `/payment/{app}/orders_without_invoice`.replace(`{${"app"}}`, encodeURIComponent(String(requestParameters.app))),
            method: 'GET',
            headers: headerParameters,
            query: queryParameters,
        }, initOverrides);

        return new runtime.JSONApiResponse(response, (jsonValue) => jsonValue.map(PaymentOrderFromJSON));
    }

    /**
     * 获取未曾开票的订单列表
     */
    async paymentOrdersWithoutInvoice(requestParameters: PaymentOrdersWithoutInvoiceRequest, initOverrides?: RequestInit | runtime.InitOverrideFunction): Promise<Array<PaymentOrder>> {
        const response = await this.paymentOrdersWithoutInvoiceRaw(requestParameters, initOverrides);
        return await response.value();
    }

    /**
     * 
     * 获取套餐价格
     */
    async paymentPriceRaw(requestParameters: PaymentPriceRequest, initOverrides?: RequestInit | runtime.InitOverrideFunction): Promise<runtime.ApiResponse<PaymentService>> {
        if (requestParameters.app === null || requestParameters.app === undefined) {
            throw new runtime.RequiredError('app','Required parameter requestParameters.app was null or undefined when calling paymentPrice.');
        }

        if (requestParameters.service === null || requestParameters.service === undefined) {
            throw new runtime.RequiredError('service','Required parameter requestParameters.service was null or undefined when calling paymentPrice.');
        }

        const queryParameters: any = {};

        if (requestParameters.service !== undefined) {
            queryParameters['service'] = requestParameters.service;
        }

        const headerParameters: runtime.HTTPHeaders = {};

        if (this.configuration && this.configuration.accessToken) {
            const token = this.configuration.accessToken;
            const tokenString = await token("TokenAuth", []);

            if (tokenString) {
                headerParameters["Authorization"] = `Bearer ${tokenString}`;
            }
        }
        const response = await this.request({
            path: `/payment/{app}/price`.replace(`{${"app"}}`, encodeURIComponent(String(requestParameters.app))),
            method: 'GET',
            headers: headerParameters,
            query: queryParameters,
        }, initOverrides);

        return new runtime.JSONApiResponse(response, (jsonValue) => PaymentServiceFromJSON(jsonValue));
    }

    /**
     * 
     * 获取套餐价格
     */
    async paymentPrice(requestParameters: PaymentPriceRequest, initOverrides?: RequestInit | runtime.InitOverrideFunction): Promise<PaymentService> {
        const response = await this.paymentPriceRaw(requestParameters, initOverrides);
        return await response.value();
    }

    /**
     * 查看回执图片
     * 获取订单回执图片
     */
    async paymentReceiptRaw(requestParameters: PaymentReceiptRequest, initOverrides?: RequestInit | runtime.InitOverrideFunction): Promise<runtime.ApiResponse<Blob>> {
        if (requestParameters.app === null || requestParameters.app === undefined) {
            throw new runtime.RequiredError('app','Required parameter requestParameters.app was null or undefined when calling paymentReceipt.');
        }

        if (requestParameters.ident === null || requestParameters.ident === undefined) {
            throw new runtime.RequiredError('ident','Required parameter requestParameters.ident was null or undefined when calling paymentReceipt.');
        }

        if (requestParameters.id === null || requestParameters.id === undefined) {
            throw new runtime.RequiredError('id','Required parameter requestParameters.id was null or undefined when calling paymentReceipt.');
        }

        const queryParameters: any = {};

        if (requestParameters.ident !== undefined) {
            queryParameters['ident'] = requestParameters.ident;
        }

        if (requestParameters.id !== undefined) {
            queryParameters['id'] = requestParameters.id;
        }

        const headerParameters: runtime.HTTPHeaders = {};

        if (this.configuration && this.configuration.accessToken) {
            const token = this.configuration.accessToken;
            const tokenString = await token("TokenAuth", []);

            if (tokenString) {
                headerParameters["Authorization"] = `Bearer ${tokenString}`;
            }
        }
        const response = await this.request({
            path: `/payment/{app}/orders`.replace(`{${"app"}}`, encodeURIComponent(String(requestParameters.app))),
            method: 'GET',
            headers: headerParameters,
            query: queryParameters,
        }, initOverrides);

        return new runtime.BlobApiResponse(response);
    }

    /**
     * 查看回执图片
     * 获取订单回执图片
     */
    async paymentReceipt(requestParameters: PaymentReceiptRequest, initOverrides?: RequestInit | runtime.InitOverrideFunction): Promise<Blob> {
        const response = await this.paymentReceiptRaw(requestParameters, initOverrides);
        return await response.value();
    }

    /**
     * 获取私有化方案、报价等信息
     * 联系销售获取私有化报价
     */
    async paymentRequestContactRaw(requestParameters: PaymentRequestContactRequest, initOverrides?: RequestInit | runtime.InitOverrideFunction): Promise<runtime.ApiResponse<boolean>> {
        if (requestParameters.app === null || requestParameters.app === undefined) {
            throw new runtime.RequiredError('app','Required parameter requestParameters.app was null or undefined when calling paymentRequestContact.');
        }

        if (requestParameters.contactForm === null || requestParameters.contactForm === undefined) {
            throw new runtime.RequiredError('contactForm','Required parameter requestParameters.contactForm was null or undefined when calling paymentRequestContact.');
        }

        const queryParameters: any = {};

        const headerParameters: runtime.HTTPHeaders = {};

        headerParameters['Content-Type'] = 'application/json';

        if (this.configuration && this.configuration.accessToken) {
            const token = this.configuration.accessToken;
            const tokenString = await token("TokenAuth", []);

            if (tokenString) {
                headerParameters["Authorization"] = `Bearer ${tokenString}`;
            }
        }
        const response = await this.request({
            path: `/payment/{app}/contact`.replace(`{${"app"}}`, encodeURIComponent(String(requestParameters.app))),
            method: 'POST',
            headers: headerParameters,
            query: queryParameters,
            body: ContactFormToJSON(requestParameters.contactForm),
        }, initOverrides);

        return new runtime.TextApiResponse(response) as any;
    }

    /**
     * 获取私有化方案、报价等信息
     * 联系销售获取私有化报价
     */
    async paymentRequestContact(requestParameters: PaymentRequestContactRequest, initOverrides?: RequestInit | runtime.InitOverrideFunction): Promise<boolean> {
        const response = await this.paymentRequestContactRaw(requestParameters, initOverrides);
        return await response.value();
    }

    /**
     * 用户在完成银行转账后，通过该接口上传转账回执
     * 上传转账回执
     */
    async paymentUploadReceiptRaw(requestParameters: PaymentUploadReceiptRequest, initOverrides?: RequestInit | runtime.InitOverrideFunction): Promise<runtime.ApiResponse<PaymentOrder>> {
        if (requestParameters.app === null || requestParameters.app === undefined) {
            throw new runtime.RequiredError('app','Required parameter requestParameters.app was null or undefined when calling paymentUploadReceipt.');
        }

        if (requestParameters.ident === null || requestParameters.ident === undefined) {
            throw new runtime.RequiredError('ident','Required parameter requestParameters.ident was null or undefined when calling paymentUploadReceipt.');
        }

        const queryParameters: any = {};

        const headerParameters: runtime.HTTPHeaders = {};

        if (this.configuration && this.configuration.accessToken) {
            const token = this.configuration.accessToken;
            const tokenString = await token("TokenAuth", []);

            if (tokenString) {
                headerParameters["Authorization"] = `Bearer ${tokenString}`;
            }
        }
        const consumes: runtime.Consume[] = [
            { contentType: 'multipart/form-data' },
        ];
        // @ts-ignore: canConsumeForm may be unused
        const canConsumeForm = runtime.canConsumeForm(consumes);

        let formParams: { append(param: string, value: any): any };
        let useForm = false;
        // use FormData to transmit files using content-type "multipart/form-data"
        useForm = canConsumeForm;
        if (useForm) {
            formParams = new FormData();
        } else {
            formParams = new URLSearchParams();
        }

        if (requestParameters.receipt !== undefined) {
            formParams.append('receipt', requestParameters.receipt as any);
        }

        const response = await this.request({
            path: `/payment/{app}/orders/{ident}`.replace(`{${"app"}}`, encodeURIComponent(String(requestParameters.app))).replace(`{${"ident"}}`, encodeURIComponent(String(requestParameters.ident))),
            method: 'PUT',
            headers: headerParameters,
            query: queryParameters,
            body: formParams,
        }, initOverrides);

        return new runtime.JSONApiResponse(response, (jsonValue) => PaymentOrderFromJSON(jsonValue));
    }

    /**
     * 用户在完成银行转账后，通过该接口上传转账回执
     * 上传转账回执
     */
    async paymentUploadReceipt(requestParameters: PaymentUploadReceiptRequest, initOverrides?: RequestInit | runtime.InitOverrideFunction): Promise<PaymentOrder> {
        const response = await this.paymentUploadReceiptRaw(requestParameters, initOverrides);
        return await response.value();
    }

    /**
     * 接受微信支付的支付结果
     */
    async paymentWepayRaw(initOverrides?: RequestInit | runtime.InitOverrideFunction): Promise<runtime.ApiResponse<void>> {
        const queryParameters: any = {};

        const headerParameters: runtime.HTTPHeaders = {};

        const response = await this.request({
            path: `/payment/wepay`,
            method: 'POST',
            headers: headerParameters,
            query: queryParameters,
        }, initOverrides);

        return new runtime.VoidApiResponse(response);
    }

    /**
     * 接受微信支付的支付结果
     */
    async paymentWepay(initOverrides?: RequestInit | runtime.InitOverrideFunction): Promise<void> {
        await this.paymentWepayRaw(initOverrides);
    }

}

/**
 * @export
 */
export const PaymentBeginPayTypeEnum = {
    None: 'none',
    Alipay: 'alipay',
    Wepay: 'wepay',
    Bank: 'bank',
    Paypal: 'paypal',
    Stripe: 'stripe'
} as const;
export type PaymentBeginPayTypeEnum = typeof PaymentBeginPayTypeEnum[keyof typeof PaymentBeginPayTypeEnum];
