/* tslint:disable */
/* eslint-disable */
/**
 * Indexea OpenAPI
 * 这是 Indexea 搜索服务平台的 OpenAPI，用于描述平台的所有接口信息，你可以通过这个页面来了解和在线验证平台的所有接口信息。  ### Errors  本 API 使用标准的 HTTP 状态码来指示操作成功或者失败，如果失败将会在 body 中以 JSON 格式提供详细的错误信息，如下所示：  ``` {   \"error\": 404,   \"message\": \"page not found\" } ``` 
 *
 * The version of the OpenAPI document: 1.0.0
 * Contact: indexea.com@gmail.com
 *
 * NOTE: This class is auto generated by OpenAPI Generator (https://openapi-generator.tech).
 * https://openapi-generator.tech
 * Do not edit the class manually.
 */

import { exists, mapValues } from '../runtime';
/**
 * 同义词定义
 * @export
 * @interface SynonymsBean
 */
export interface SynonymsBean {
    /**
     * 
     * @type {number}
     * @memberof SynonymsBean
     */
    id?: number;
    /**
     * 
     * @type {number}
     * @memberof SynonymsBean
     */
    app?: number;
    /**
     * 
     * @type {number}
     * @memberof SynonymsBean
     */
    index?: number;
    /**
     * 
     * @type {number}
     * @memberof SynonymsBean
     */
    type?: number;
    /**
     * 
     * @type {Array<string>}
     * @memberof SynonymsBean
     */
    words?: Array<string>;
    /**
     * 
     * @type {Array<string>}
     * @memberof SynonymsBean
     */
    synonyms?: Array<string>;
    /**
     * 
     * @type {Date}
     * @memberof SynonymsBean
     */
    updatedAt?: Date;
    /**
     * 
     * @type {number}
     * @memberof SynonymsBean
     */
    status?: number;
}

/**
 * Check if a given object implements the SynonymsBean interface.
 */
export function instanceOfSynonymsBean(value: object): boolean {
    let isInstance = true;

    return isInstance;
}

export function SynonymsBeanFromJSON(json: any): SynonymsBean {
    return SynonymsBeanFromJSONTyped(json, false);
}

export function SynonymsBeanFromJSONTyped(json: any, ignoreDiscriminator: boolean): SynonymsBean {
    if ((json === undefined) || (json === null)) {
        return json;
    }
    return {
        
        'id': !exists(json, 'id') ? undefined : json['id'],
        'app': !exists(json, 'app') ? undefined : json['app'],
        'index': !exists(json, 'index') ? undefined : json['index'],
        'type': !exists(json, 'type') ? undefined : json['type'],
        'words': !exists(json, 'words') ? undefined : json['words'],
        'synonyms': !exists(json, 'synonyms') ? undefined : json['synonyms'],
        'updatedAt': !exists(json, 'updated_at') ? undefined : (new Date(json['updated_at'])),
        'status': !exists(json, 'status') ? undefined : json['status'],
    };
}

export function SynonymsBeanToJSON(value?: SynonymsBean | null): any {
    if (value === undefined) {
        return undefined;
    }
    if (value === null) {
        return null;
    }
    return {
        
        'id': value.id,
        'app': value.app,
        'index': value.index,
        'type': value.type,
        'words': value.words,
        'synonyms': value.synonyms,
        'updated_at': value.updatedAt === undefined ? undefined : (value.updatedAt.toISOString()),
        'status': value.status,
    };
}

