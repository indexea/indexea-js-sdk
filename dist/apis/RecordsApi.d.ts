/**
 * Indexea OpenAPI
 * 这是 Indexea 搜索服务平台的 OpenAPI，用于描述平台的所有接口信息，你可以通过这个页面来了解和在线验证平台的所有接口信息。  ### Errors  本 API 使用标准的 HTTP 状态码来指示操作成功或者失败，如果失败将会在 body 中以 JSON 格式提供详细的错误信息，如下所示：  ``` {   \"error\": 404,   \"message\": \"page not found\" } ```
 *
 * The version of the OpenAPI document: 1.0.0
 * Contact: indexea.com@gmail.com
 *
 * NOTE: This class is auto generated by OpenAPI Generator (https://openapi-generator.tech).
 * https://openapi-generator.tech
 * Do not edit the class manually.
 */
import * as runtime from '../runtime';
import type { RecordFilter } from '../models';
export interface RecordCountBeforeTimeRequest {
    app: string;
    index: number;
    timestamp: number;
}
export interface RecordDeleteRequest {
    app: string;
    index: number;
    id: Array<string>;
}
export interface RecordDeleteBeforeTimeRequest {
    app: string;
    index: number;
    timestamp: number;
}
export interface RecordDeleteByQueryRequest {
    app: string;
    index: number;
    query: number;
    params?: {
        [key: string]: string;
    };
}
export interface RecordGetRequest {
    app: string;
    index: number;
    id: string;
}
export interface RecordListRequest {
    app: string;
    index: number;
    q?: string;
    field?: string;
    from?: number;
    size?: number;
    saveFilter?: boolean;
    recordFilter?: RecordFilter;
}
export interface RecordPushRequest {
    app: string;
    index: number;
    requestBody: Array<object>;
    combine?: string;
}
export interface RecordUpdateByQueryRequest {
    app: string;
    index: number;
    query: number;
    body: object;
    params?: {
        [key: string]: string;
    };
}
export interface RecordUploadRequest {
    app: string;
    index: number;
    combine?: boolean;
    useIdAsIdValue?: boolean;
    files?: Array<Blob>;
}
export interface RecordUploadOldRequest {
    app: string;
    index: number;
    combine?: boolean;
    useIdAsIdValue?: boolean;
    files?: Array<Blob>;
}
/**
 *
 */
export declare class RecordsApi extends runtime.BaseAPI {
    /**
     * 通过 timestamp 参数获取指定时间之前的记录数。
     * 查询指定时间之前的日志数据记录数
     */
    recordCountBeforeTimeRaw(requestParameters: RecordCountBeforeTimeRequest, initOverrides?: RequestInit | runtime.InitOverrideFunction): Promise<runtime.ApiResponse<object>>;
    /**
     * 通过 timestamp 参数获取指定时间之前的记录数。
     * 查询指定时间之前的日志数据记录数
     */
    recordCountBeforeTime(requestParameters: RecordCountBeforeTimeRequest, initOverrides?: RequestInit | runtime.InitOverrideFunction): Promise<object>;
    /**
     * 删除记录数据
     */
    recordDeleteRaw(requestParameters: RecordDeleteRequest, initOverrides?: RequestInit | runtime.InitOverrideFunction): Promise<runtime.ApiResponse<boolean>>;
    /**
     * 删除记录数据
     */
    recordDelete(requestParameters: RecordDeleteRequest, initOverrides?: RequestInit | runtime.InitOverrideFunction): Promise<boolean>;
    /**
     * 通过 timestamp 参数删除指定时间之前的记录（该参数仅用于日志索引数据的删除），该操作要求管理员权限。
     * 删除指定时间之前的日志数据
     */
    recordDeleteBeforeTimeRaw(requestParameters: RecordDeleteBeforeTimeRequest, initOverrides?: RequestInit | runtime.InitOverrideFunction): Promise<runtime.ApiResponse<boolean>>;
    /**
     * 通过 timestamp 参数删除指定时间之前的记录（该参数仅用于日志索引数据的删除），该操作要求管理员权限。
     * 删除指定时间之前的日志数据
     */
    recordDeleteBeforeTime(requestParameters: RecordDeleteBeforeTimeRequest, initOverrides?: RequestInit | runtime.InitOverrideFunction): Promise<boolean>;
    /**
     * 批量删除记录数据(delete_by_query)，该接口传递查询参数获取要删除的记录并逐一删除
     */
    recordDeleteByQueryRaw(requestParameters: RecordDeleteByQueryRequest, initOverrides?: RequestInit | runtime.InitOverrideFunction): Promise<runtime.ApiResponse<void>>;
    /**
     * 批量删除记录数据(delete_by_query)，该接口传递查询参数获取要删除的记录并逐一删除
     */
    recordDeleteByQuery(requestParameters: RecordDeleteByQueryRequest, initOverrides?: RequestInit | runtime.InitOverrideFunction): Promise<void>;
    /**
     * 获取单条记录详情
     */
    recordGetRaw(requestParameters: RecordGetRequest, initOverrides?: RequestInit | runtime.InitOverrideFunction): Promise<runtime.ApiResponse<object>>;
    /**
     * 获取单条记录详情
     */
    recordGet(requestParameters: RecordGetRequest, initOverrides?: RequestInit | runtime.InitOverrideFunction): Promise<object>;
    /**
     * 获取索引记录列表
     */
    recordListRaw(requestParameters: RecordListRequest, initOverrides?: RequestInit | runtime.InitOverrideFunction): Promise<runtime.ApiResponse<Array<object>>>;
    /**
     * 获取索引记录列表
     */
    recordList(requestParameters: RecordListRequest, initOverrides?: RequestInit | runtime.InitOverrideFunction): Promise<Array<object>>;
    /**
     * 插入或者更新索引数据
     */
    recordPushRaw(requestParameters: RecordPushRequest, initOverrides?: RequestInit | runtime.InitOverrideFunction): Promise<runtime.ApiResponse<boolean>>;
    /**
     * 插入或者更新索引数据
     */
    recordPush(requestParameters: RecordPushRequest, initOverrides?: RequestInit | runtime.InitOverrideFunction): Promise<boolean>;
    /**
     * 批量修改记录数据(update_by_query)，该接口传递查询参数获取要更新的记录，并使用 body 中的对象进行记录合并更新
     */
    recordUpdateByQueryRaw(requestParameters: RecordUpdateByQueryRequest, initOverrides?: RequestInit | runtime.InitOverrideFunction): Promise<runtime.ApiResponse<void>>;
    /**
     * 批量修改记录数据(update_by_query)，该接口传递查询参数获取要更新的记录，并使用 body 中的对象进行记录合并更新
     */
    recordUpdateByQuery(requestParameters: RecordUpdateByQueryRequest, initOverrides?: RequestInit | runtime.InitOverrideFunction): Promise<void>;
    /**
     * 上传记录
     */
    recordUploadRaw(requestParameters: RecordUploadRequest, initOverrides?: RequestInit | runtime.InitOverrideFunction): Promise<runtime.ApiResponse<object>>;
    /**
     * 上传记录
     */
    recordUpload(requestParameters: RecordUploadRequest, initOverrides?: RequestInit | runtime.InitOverrideFunction): Promise<object>;
    /**
     * 上传记录，该接口已经废弃，请使用 /records/{app}/{index}/bulk 接口
     */
    recordUploadOldRaw(requestParameters: RecordUploadOldRequest, initOverrides?: RequestInit | runtime.InitOverrideFunction): Promise<runtime.ApiResponse<object>>;
    /**
     * 上传记录，该接口已经废弃，请使用 /records/{app}/{index}/bulk 接口
     */
    recordUploadOld(requestParameters: RecordUploadOldRequest, initOverrides?: RequestInit | runtime.InitOverrideFunction): Promise<object>;
}
