/* tslint:disable */
/* eslint-disable */
/**
 * Indexea OpenAPI
 * 这是 Indexea 搜索服务平台的 OpenAPI，用于描述平台的所有接口信息，你可以通过这个页面来了解和在线验证平台的所有接口信息。  ### Errors  本 API 使用标准的 HTTP 状态码来指示操作成功或者失败，如果失败将会在 body 中以 JSON 格式提供详细的错误信息，如下所示：  ``` {   \"error\": 404,   \"message\": \"page not found\" } ``` 
 *
 * The version of the OpenAPI document: 1.0.0
 * Contact: indexea.com@gmail.com
 *
 * NOTE: This class is auto generated by OpenAPI Generator (https://openapi-generator.tech).
 * https://openapi-generator.tech
 * Do not edit the class manually.
 */

import { exists, mapValues } from '../runtime';
/**
 * 索引统计信息
 * @export
 * @interface IndexStatBean
 */
export interface IndexStatBean {
    /**
     * 
     * @type {string}
     * @memberof IndexStatBean
     */
    uuid?: string;
    /**
     * 
     * @type {string}
     * @memberof IndexStatBean
     */
    index?: string;
    /**
     * 
     * @type {string}
     * @memberof IndexStatBean
     */
    health?: string;
    /**
     * 
     * @type {string}
     * @memberof IndexStatBean
     */
    status?: string;
    /**
     * 
     * @type {number}
     * @memberof IndexStatBean
     */
    records?: number;
    /**
     * 
     * @type {number}
     * @memberof IndexStatBean
     */
    storage?: number;
    /**
     * 
     * @type {number}
     * @memberof IndexStatBean
     */
    shards?: number;
    /**
     * 
     * @type {number}
     * @memberof IndexStatBean
     */
    replicas?: number;
}

/**
 * Check if a given object implements the IndexStatBean interface.
 */
export function instanceOfIndexStatBean(value: object): boolean {
    let isInstance = true;

    return isInstance;
}

export function IndexStatBeanFromJSON(json: any): IndexStatBean {
    return IndexStatBeanFromJSONTyped(json, false);
}

export function IndexStatBeanFromJSONTyped(json: any, ignoreDiscriminator: boolean): IndexStatBean {
    if ((json === undefined) || (json === null)) {
        return json;
    }
    return {
        
        'uuid': !exists(json, 'uuid') ? undefined : json['uuid'],
        'index': !exists(json, 'index') ? undefined : json['index'],
        'health': !exists(json, 'health') ? undefined : json['health'],
        'status': !exists(json, 'status') ? undefined : json['status'],
        'records': !exists(json, 'records') ? undefined : json['records'],
        'storage': !exists(json, 'storage') ? undefined : json['storage'],
        'shards': !exists(json, 'shards') ? undefined : json['shards'],
        'replicas': !exists(json, 'replicas') ? undefined : json['replicas'],
    };
}

export function IndexStatBeanToJSON(value?: IndexStatBean | null): any {
    if (value === undefined) {
        return undefined;
    }
    if (value === null) {
        return null;
    }
    return {
        
        'uuid': value.uuid,
        'index': value.index,
        'health': value.health,
        'status': value.status,
        'records': value.records,
        'storage': value.storage,
        'shards': value.shards,
        'replicas': value.replicas,
    };
}

